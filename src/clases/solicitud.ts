export class Solicitud {

    private solicitudBody: any = {
        descripcion: "",
        longitud: "",
        latitud: "",
        direccion: "",
        fechaSolicitud: "",
        servicioId: {
            servicioId: null
        },
        usuarioIdProveedor: {
            usuarioId: null
        },
        usuarioIdCliente: {
            usuarioId: null
        }
    };

    public setSolicitudBody(formulario: any, geolocalizacion: any, servicio: any, usuarioId:number): void {
        this.solicitudBody.descripcion = formulario.descripcion;
        this.solicitudBody.direccion = formulario.direccion.trim();
        this.solicitudBody.fechaSolicitud = formulario.fechaSolicitud + "T" + formulario.horaSolicitud + ":36.000Z";
        this.solicitudBody.latitud = geolocalizacion.latitud;
        this.solicitudBody.longitud = geolocalizacion.longitud;
        this.solicitudBody.servicioId.servicioId = +servicio.servicioId;
        this.solicitudBody.usuarioIdCliente.usuarioId = usuarioId;
    }

    public getSolicitudBody(): any {
        return this.solicitudBody;
    }


}