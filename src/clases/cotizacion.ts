
export class Cotizacion {

    private cotizacionBody: any = {
        valor: 0,
        fechaEstimada: "",
        duracion: 0,
        solicitudId: {
            solicitudId: null
        },
        usuarioIdProveedor: {
            usuarioId: null
        }
    };

    public setCotizacionBody(formulario: any, datosSession: any) {
        this.cotizacionBody.valor = formulario.costoServicio;
        this.cotizacionBody.fechaEstimada = formulario.fechaSolicitud + "T" + formulario.horaSolicitud + ":36.000Z";
        this.cotizacionBody.duracion = formulario.duracion;
        this.cotizacionBody.solicitudId.solicitudId = datosSession.solicitudId;
        this.cotizacionBody.usuarioIdProveedor.usuarioId = datosSession.usuarioIdProveedor;
    }

    public getCotizacionBody(): any {
        return this.cotizacionBody;
    }
}