import { AlertController, Alert } from 'ionic-angular';
import { FormGroup } from '@angular/forms';

export class validarFormularios {

  /**
 * @author Jorge Leiva
 * @version 1.0.0
 * @description Procedimiento para la validación de los formularios
 * @param formulario Formulario reactivo
 * @returns variable formulario
 */
  public validarFormulariosService(formulario: FormGroup) {
    let controles = formulario.controls;
    for (let x in controles) {
      controles[x].markAsDirty();
    }
    return controles;
  }

  /**
   * @author Jorge Leiva
   * @version 1.0.0
   * @description Procedimiento validar el formulario
   * @param controles Controles del formulario
   * @returns variable booleana
   */
  public isValidFormulario(controles: any): boolean {
    let acumulador: boolean = true;
    for (let x in controles) {
      if (controles[x].invalid == true) {
        acumulador = false;
        break;
      }
    }
    return acumulador;
  }


  public crearAlerta(alert:AlertController,  titulo: string = "", subtitulo: string = ""): Alert {
    const alertas = alert.create({
      title: titulo,
      subTitle: subtitulo,
      buttons: ['ACEPTAR']
    });
    return alertas;
  }


}