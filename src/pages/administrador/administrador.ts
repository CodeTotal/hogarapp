import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController, AlertController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { validarFormularios } from "../../clases/validarFormulario";
import { InicioSesionPage } from "../inicio-sesion/inicio-sesion";
import { File } from '@ionic-native/file';


@IonicPage()
@Component({
  selector: 'page-administrador',
  templateUrl: 'administrador.html',
})
export class AdministradorPage {

  public formulario: FormGroup;
  private loader: any;
  private ruta: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private fb: FormBuilder, private loadingCtrl: LoadingController, private alert: AlertController, private file: File) {
    this.formulario = this.fb.group({
      ip: ['', Validators.required],
      puerto: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('Página de administración cargada');
    this.ruta = this.file.dataDirectory + "configuracion";
    this.file.readAsText(this.ruta, "rootHogarapp").then(leer => {
      let atributosConexion = leer.split("|");
      this.formulario.get("ip").setValue(atributosConexion[0].replace(":", ""));
      this.formulario.get("puerto").setValue(atributosConexion[1].replace("/", ""));
    }).catch(err => {
      alert(err);
    });
  }

  loading() {
    this.loader = this.loadingCtrl.create({
      content: "Espera..."
    });
    this.loader.present();
  }

  regresarInicioSesion() {
    this.navCtrl.setRoot(InicioSesionPage, {}, { animate: true });
  }

  asignarConfiguacion() {
    let validaciones = new validarFormularios();
    this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
    let isValid: boolean = validaciones.isValidFormulario(this.formulario.controls);
    if (isValid) {
      this.loading();
      console.log("verificacion de los campos --> verdadera");
      let datosConfiguracion = this.formulario.get("ip").value + ":|" + this.formulario.get("puerto").value + "/";
      this.file.writeFile(this.ruta, "rootHogarapp", datosConfiguracion, { replace: true }).then(escritura => {
        console.log("archivo escrito" + escritura);
        this.loader.dismiss();
        this.navCtrl.setRoot(InicioSesionPage);
      }).catch(err => {
        console.log(err);
        this.loader.dismiss();
        validaciones.crearAlerta(this.alert, "error", "Error creando el archivo de configuación");
      });
    }
  }
}
