import { NavParams, Tabs } from 'ionic-angular';
import { Component, ViewChild } from '@angular/core';

import { ConfiguracionPage } from '../Configuraciones/about';
import { SolicitudPage } from '../Solicitudes/contact';
import { InicioPage } from '../Inicio/home';

@Component({
    templateUrl: 'tabs.html'
})
export class TabsPage {

    @ViewChild("opcionesApp") opcionesApp: Tabs;

    tab1Root = null;
    tab2Root = SolicitudPage;
    tab3Root = ConfiguracionPage;

    tituloPaginaInicio: string = "Inicio";
    tituloPaginaSolicitudes: string = "Solicitudes";
    tituloPaginaConfiguracion: string = "Perfil";
    iconoServicio: string = "chatbubbles";
    selectedIndexTabs: number = 0;
    private esValido: boolean;
    public indice: number;

    constructor(private navParams: NavParams) {
    }

    ionViewDidLoad() {
        // Se ejecuta cuando se crea una nueva solicitud
        let selectedIndexTabs: number = this.navParams.get("solicitudCreada");
        if (selectedIndexTabs !== null) this.opcionesApp.select(selectedIndexTabs);
        let tipoUsuario = this.navParams.get("tipoUsuario");
        console.log("TipoUsuario", tipoUsuario);
        this.esValido = this.validarUsuario(tipoUsuario);
        if (this.esValido) {
            console.log("Entra como proveedor");
            this.iconoServicio = "paper";
            this.tituloPaginaSolicitudes = "Buzón";
            this.indice = 1;
        } else {
            console.log("entra como usuario");
        }
    }

    ionViewCanEnter() {
        let tipoUsuario = this.navParams.get("tipoUsuario");
        this.esValido = this.validarUsuario(tipoUsuario);
        if (!this.esValido) {
            this.tab1Root = InicioPage;
        } else {
            this.opcionesApp._tabs[0].show = false;
        }
    }

    private validarUsuario(tipoUsuario: any): boolean {
        let respuesta: boolean = false;
        if (tipoUsuario == "1") respuesta = true; //tipo de usuario Proveedor
        return respuesta;
    }

}
