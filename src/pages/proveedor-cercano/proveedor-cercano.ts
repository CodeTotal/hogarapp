import { AlertController } from 'ionic-angular';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { ServiciosServicioGeneralProvider } from "../../providers/servicios-servicio-general/servicios-servicio-general";
import { Storage } from '@ionic/storage';
import { TabsPage } from '../tabs/tabs';

declare var $: any;
declare var H: any;

@IonicPage()
@Component({
    selector: 'page-proveedor-cercano',
    templateUrl: 'proveedor-cercano.html',
})
export class ProveedorCercanoPage {

    public direccionesObtenidas: any[] = [];
    private platform: any;
    private defaultLayers: any;
    private map: any;
    private behavior: any;
    private ui: any;
    private group: any;
    public latitud: string = "";
    public longitud: string = "";
    private loader: any;
    private solicitudCliente: any = {};
    private listaProveedores: any[] = [];
    private informacion: any;

    constructor(
        private navParams: NavParams,
        private loadingCtrl: LoadingController,
        private navCrt: NavController,
        private servicio: ServiciosServicioGeneralProvider,
        private alert: AlertController,
        private storage: Storage
    ) {
    }

    notificarTodosProveedores() {
        console.log("Cotización individual de proveedores realizada");
        this.solicitudCliente.usuarioIdProveedor = null;
        this.cotizarProveedores();
    }

    loading() {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    }

    private conversionCoordenadas() {
        let latitud: string = "" + this.solicitudCliente.latitud;
        let longitud: string = "" + this.solicitudCliente.longitud;
        this.solicitudCliente.latitud = latitud.replace(".", ",");
        this.solicitudCliente.longitud = longitud.replace(".", ",");
    }

    
    ionViewDidLoad() {
        console.log('Cargando mapa...');
        this.solicitudCliente = this.navParams.get("solicitud");
        this.conversionCoordenadas();
        console.log("SolicitudCliente por parámetros ---->>> ", this.solicitudCliente);
        this.loading();
        let longitud: string = "" + this.solicitudCliente.longitud;
        let latitud: string = "" + this.solicitudCliente.latitud;
        let servicio: string = this.solicitudCliente.servicioId.servicioId;
        let parametros = "?longitud=" + longitud.replace(".", ",") + "&latitud=" + latitud.replace(".", ",") + "&servicioId=" + servicio;
        this.servicio.getDataResource("obtenerProveedoresCercanos" + parametros).subscribe(proveedores => {
            console.log("Estos son los proveedores->> ", proveedores);
            if (proveedores.status == 200) {
                let conversion: any = proveedores.body;
                this.listaProveedores = conversion;
                this.construirMapa();
            }
        }, err => {
            console.error(err);
        });
    }

    private construirMapa() {
        this.platform = new H.service.Platform({
            app_id: 'nhOznJTavPjLeA0U3dGS',
            app_code: 'qBiDEC4S0UqZPi7Ou5X1UQ',
            //useCIT: false,
            useHTTPS: true
        });

        this.defaultLayers = this.platform.createDefaultLayers({
            tileSize: 512,
            ppi: 320
        });
        this.group = new H.map.Group();

        this.map = new H.Map(document.getElementById('map'),
            this.defaultLayers.normal.map, {
                center: { lat: 4.6022608, lng: -74.0897502 },
                zoom: 12
            });

        this.map.addObject(this.group);
        this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));
        this.ui = H.ui.UI.createDefault(this.map, this.defaultLayers);
        console.log(this.ui);
        console.log(this.behavior);
        this.ubicarProveedoresMapa();
    }

    private ubicarProveedoresMapa() {
        var group = this.group;
        group.addEventListener('tap', function (evt) {
            console.log("evento del tap ---> ", evt.target.getData());
            $('#tarjetaProveedor').data('informacion', evt.target.getData());
            $('#tarjetaProveedor').click();
        }, false);
        this.addMarkerToGroup(group);
        this.loader.dismiss();
    }

    public tarjetaProveedor() {
        this.informacion = $('#tarjetaProveedor').data('informacion');
        const alert = this.alert.create({
            title: 'Proveedor',
            subTitle: this.informacion.nombres + " " + this.informacion.apellidos,
            buttons: [{
                text: 'COTIZAR',
                handler: () => {
                    console.log("Usuario lo ha cotizado");
                    this.cotizarProveedores();
                }
            }, {
                text: 'CANCELAR',
                role: 'cancel',
                handler: () => {
                    console.log("acción cancelada");
                }
            }]
        });
        alert.present();
    }


    private cotizarProveedores() {
        this.loading();
        console.log("Cotización proveedores realizada");
        if (this.solicitudCliente.usuarioIdProveedor !== null) {
            if (this.solicitudCliente.usuarioIdProveedor.usuarioId == null) this.solicitudCliente.usuarioIdProveedor.usuarioId = this.informacion.usuarioId;
        }
        this.servicio.sendDataResource(this.solicitudCliente, "crearSolicitud").subscribe(solProveedor => {
            let listadoSolicitudesLocales: any[] = [];
            console.info("Enviando datos solicitud proveedores -> ", solProveedor);
            this.storage.get("solicitudesCliente").then(data => {
                if (data == null) {
                    listadoSolicitudesLocales.push(solProveedor);
                    this.storage.set("solicitudesCliente", listadoSolicitudesLocales);
                } else {
                    listadoSolicitudesLocales = data;
                    listadoSolicitudesLocales.push(solProveedor);
                    this.storage.set("solicitudesCliente", listadoSolicitudesLocales);
                }
                this.loader.dismiss();
                this.navCrt.setRoot(TabsPage, { solicitudCreada: 1 });
            }).catch(err => console.log(err));
        }, err => {
            console.error(err);
            this.loader.dismiss();
        });
    }

    private addMarkerToGroup(group: any) {
        var imagenUsuario = document.getElementById('imagenIconoUsuario');
        var iterador = this.listaProveedores;
        var marker, latitud, longitud;
        for (var index = 0; index < iterador.length; index++) {
            latitud = +iterador[index].coordenadaId.latitud.replace(",", ".");
            longitud = +iterador[index].coordenadaId.longitud.replace(",", ".");
            marker = new H.map.Marker({
                lat: latitud,
                lng: longitud
            }, { icon: new H.map.Icon(imagenUsuario) });
            marker.setData(iterador[index]);
            group.addObject(marker);
        }
    }

}
