import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProveedorCercanoPage } from './proveedor-cercano';

@NgModule({
  declarations: [
    ProveedorCercanoPage,
  ],
  imports: [
    IonicPageModule.forChild(ProveedorCercanoPage),
  ],
})
export class ProveedorCercanoPageModule {}
