import { InicioSesionPage } from './../inicio-sesion/inicio-sesion';
import { Component } from '@angular/core';
import { NavController, LoadingController, App } from 'ionic-angular';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'page-about',
  templateUrl: 'about.html'
})

export class ConfiguracionPage {

  imagenUsuario: string = "/assets/imgs/perfilusuario3.png";
  datosUsuario: any = {
    nombres: "",
    apellidos: "",
    direccion: "",
    telefono: "",
    estado: ""
  };
  public loader: any;

  constructor(public navCtrl: NavController, private storage: Storage, private loadingCtrl: LoadingController, public app: App) {

  }

  loading() {
    this.loader = this.loadingCtrl.create({
      content: "Espera..."
    });
    this.loader.present();
  }

  ionViewWillEnter() {
    this.storage.get("datosUsuario").then(datosUsuario => {
      console.log("datosUsuario -> Perfil", datosUsuario);
      if (datosUsuario !== null) {
        if (datosUsuario.rolId.rolId == 2 || datosUsuario.rolId.descripcion.toUpperCase() !== "CLIENTE") this.imagenUsuario = "/assets/imgs/perfilProveedor3.png";
        this.datosUsuario = datosUsuario;
      }
    }).catch(err => console.error(err));
  }


  cerrarSesion() {
    this.loading();
    this.storage.remove("datosUsuario").then(() => {
      console.log("Elimina datos del usuario al salir");
      this.storage.remove("logueo").then(() => {
        console.log("variables locales eliminadas al salir");
        this.loader.dismiss();
        this.app.getRootNavs()[0].push(InicioSesionPage);
      })
    }).catch(err => console.error(err));
  }

}
