import { ServiciosServicioGeneralProvider } from './../../providers/servicios-servicio-general/servicios-servicio-general';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { RegistroUsuarioPage } from '../registro-usuario/registro-usuario';
import { TabsPage } from "../tabs/tabs";
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { validarFormularios } from "../../clases/validarFormulario";
import { Storage } from '@ionic/storage';
import { AdministradorPage } from "../administrador/administrador";

@IonicPage()
@Component({
    selector: 'page-inicio-sesion',
    templateUrl: 'inicio-sesion.html',
})
export class InicioSesionPage {

    public formulario: FormGroup;
    public loader: any;
    public error: any;
    public exito: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, private servicio: ServiciosServicioGeneralProvider, private fb: FormBuilder, private alert: AlertController, private loadingCtrl: LoadingController, private storage: Storage) {
        this.formulario = this.fb.group({
            usuario: ['', Validators.required],
            contrasenia: ['', Validators.required]
        });
    }

    ionViewDidLoad() {
        console.log('Inicio de sesion cargado');
    }

    registrarUsuario() {
        this.navCtrl.push(RegistroUsuarioPage, {}, {
            animation: "ios-transition"
        });
    }

    loading() {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    }

    private cargarVistaAdministrador(datosUsuario: any, validaciones: any) {
        if (datosUsuario.usuario == "admin" && datosUsuario.contrasenia == "@dmin$$") {
            this.navCtrl.push(AdministradorPage);
            this.loader.dismiss();
        } else {
            this.servicio.getDataResource("buscarUsuarioPorCorreo/", datosUsuario.usuario).subscribe(data => {
                let datos: any = data.body;
                console.log("usuario-> ", datos);
                if ((data.status == 200 && datos.usuarioId !== null) && (datos.correo == datosUsuario.usuario && datos.password == datosUsuario.contrasenia)) {
                    this.storage.clear();
                    this.storage.set('datosUsuario', datos);
                    this.storage.set("logueado", 1);
                    this.navCtrl.setRoot(TabsPage, {tipoUsuario: datos.descripcionPerfil, solicitudCreada: 0});
                } else {
                    const alert = validaciones.crearAlerta(this.alert, "Error de credenciales", "Los datos ingresados no son válidos");
                    alert.present();
                    this.formulario.reset();
                }
            }, err => {
                console.log(err);
                this.error = err;
                const alertError = validaciones.crearAlerta(this.alert, "Error de conexión", err.message);
                alertError.present();
                this.loader.dismiss();
            }, () => {
                this.loader.dismiss();
            });
        }
    }

    goVistaPrincipal() {
        // this.navCtrl.setRoot(TabsPage);
        let validaciones = new validarFormularios();
        this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
        let isValid: boolean = validaciones.isValidFormulario(this.formulario.controls);
        let datosUsuario = this.formulario.value;
        if (isValid) {
            this.loading();
            this.cargarVistaAdministrador(datosUsuario, validaciones);
        }
    }
}
