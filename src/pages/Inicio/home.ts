import { Component } from '@angular/core';
import { NavController, LoadingController } from 'ionic-angular';
import { ServiciosServicioGeneralProvider } from "../../providers/servicios-servicio-general/servicios-servicio-general";
import { ListaserviciosPage } from "../listaservicios/listaservicios";

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class InicioPage {

  obtenerValoresCategoria: any[] = [];
  error: any[] = [];
  loader: any;

  constructor(public navCtrl: NavController, private http: ServiciosServicioGeneralProvider, private loadingCtrl: LoadingController) {
  }

  ionViewWillEnter() {
    this.loading();
    // alert(this.http.getConfiguracion());
    this.http.getDataResource("obtenerCategorias").subscribe((datos) => {
      console.log(datos.body);
      let bodyResponse: any = datos.body;
      let iterable: any[] = bodyResponse;
      this.obtenerValoresCategoria = iterable;
      for (let index = 0; index < iterable.length; index++) {
        let cantidadServicios = iterable[index].servicioList.length;
        iterable[index].cantidadServicios = cantidadServicios;
      }
    }, err => {
      console.log(err);
      this.error.push(err.message);
      this.error.push(err.name);
      this.error.push(err.status);
      this.error.push(err.url);
      this.loader.dismiss();
    }, () => {
      this.loader.dismiss();
    });
  }

  loading() {
    this.loader = this.loadingCtrl.create({
      content: "Espera..."
    });
    this.loader.present();
  }

  listarServicios(indice: number) {
    console.log(this.obtenerValoresCategoria[indice]);
    let serviciosAsociados: any[] = this.obtenerValoresCategoria[indice].servicioList;
    if (serviciosAsociados.length > 0) {
      this.navCtrl.push(ListaserviciosPage, { servicios: serviciosAsociados }, {
        animation: "ios-transition"
      });
    }
  }
}
