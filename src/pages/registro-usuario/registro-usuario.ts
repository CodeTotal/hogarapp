import { TabsPage } from './../tabs/tabs';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Modal, AlertController, LoadingController } from 'ionic-angular';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { validarFormularios } from "../../clases/validarFormulario";
import { ServiciosServicioGeneralProvider } from '../../providers/servicios-servicio-general/servicios-servicio-general';
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
  selector: 'page-registro-usuario',
  templateUrl: 'registro-usuario.html',
})
export class RegistroUsuarioPage {

  public formulario: FormGroup;
  private latitud: string = "";
  private longitud: string = "";
  private idServicios: any[] = [];
  private loader: any;
  private pinSeleccionado: boolean = false;
  private direccionCompleta: string = "";

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private modal: ModalController,
    private servicio: ServiciosServicioGeneralProvider,
    private alert: AlertController,
    private loadingCtrl: LoadingController,
    private storage: Storage,
    private geolocalizacion: Geolocation
  ) {
    this.formulario = this.fb.group({
      descripcionPerfil: ['', Validators.required],
      nombres: ['', Validators.required],
      apellidos: ['', Validators.required],
      correo: ['', Validators.compose([
        Validators.required, Validators.email
      ])],
      direccion: ['', Validators.required],
      telefono: ['', Validators.required],
      password: ['', Validators.required],
      servicios: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('Pagina de registro activada');
    this.geolocalizacion.getCurrentPosition().then(posicion => {
      this.latitud = "" + posicion.coords.latitude;
      this.longitud = "" + posicion.coords.longitude;
      this.pinSeleccionado = true;
      console.log("coordenadas obtenidas-> ", posicion.coords);
    }).catch(err => {
      console.error(err);
    });
  }

  regresarPaginaPrincipal() {
    this.navCtrl.pop();
  }

  loading() {
    this.loader = this.loadingCtrl.create({
      content: "Espera..."
    });
    this.loader.present();
  }

  enviarDatosPersonales() {
    // debugger;
    let validaciones = new validarFormularios();
    let enlace: string = "crearProveedor";
    this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
    if (this.formulario.get('descripcionPerfil').value == 2) {
      this.formulario.controls.servicios.setValue(" ");
      enlace = "crearCliente";
    }
    let isValid: boolean = validaciones.isValidFormulario(this.formulario.controls);

    if (isValid) {
      this.loading();
      let datosEnviados = this.formulario.value;
      let latitud: string = "" + this.latitud;
      let longitud: string = "" + this.longitud;
      datosEnviados.coordenadaId = {
        latitud: latitud.replace(".", ","),
        longitud: longitud.replace(".", ",")
      };

      datosEnviados.username = "";
      datosEnviados.servicesId = [];
      datosEnviados.identificacion = "12334678";
      (this.formulario.get("servicios").value == " ") ? "" : datosEnviados.servicesId = this.idServicios;
      this.servicio.sendDataResource(datosEnviados, enlace).subscribe(data => {
        let datos: any = data;
        this.storage.clear();
        this.storage.set("datosUsuario", datos);
        this.navCtrl.setRoot(TabsPage, { tipoUsuario: this.formulario.get("descripcionPerfil").value });
      }, err => {
        console.error(err);
        const alerta = validaciones.crearAlerta(this.alert, "Error", err.error);
        alerta.present();
        this.loader.dismiss();
      }, () => {
        this.loader.dismiss();
      });

    } else {
      console.log(this.formulario);
    }
  }

  SoloLectura() {
    return true;
  }

  abrirModal() {
    const modalMapa: Modal = this.modal.create('MapasPage', {
      pin: this.pinSeleccionado,
      latitud: this.latitud,
      longitud: this.longitud,
      direccionCompleta: this.direccionCompleta
    });
    modalMapa.present();
    modalMapa.onDidDismiss((datos) => {
      console.log("coordenadasRecuperadas", datos);
      if (datos !== undefined) {
        this.latitud = datos.latitud;
        this.longitud = datos.longitud;
        this.formulario.get('direccion').setValue(datos.label);
        this.pinSeleccionado = datos.pin;
        this.direccionCompleta = datos.label;
      }
    });
  }

  abrirModalServicios() {
    const modalMapaServicio: Modal = this.modal.create('ProveedorServiciosPage', { servicios: this.idServicios });
    modalMapaServicio.present();
    modalMapaServicio.onDidDismiss((datos) => {
      console.log("datosRecibidos->> ", datos);
      if (datos !== undefined && datos.cantidadServicios > 0) {
        this.formulario.get("servicios").setValue(datos.cantidadServicios + " servicios");
        this.idServicios = datos.idServicios;
      } else this.formulario.get("servicios").reset();
    });
  }

}
