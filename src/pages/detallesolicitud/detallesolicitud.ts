import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavParams, Modal, ModalController, TextInput, NavController } from 'ionic-angular';
import { Validators, FormBuilder, FormGroup } from "@angular/forms";
import { ProveedorCercanoPage } from "../proveedor-cercano/proveedor-cercano";
import { validarFormularios } from '../../clases/validarFormulario';
import { Solicitud } from "../../clases/solicitud";
import { Storage } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';

@IonicPage()
@Component({
    selector: 'page-detallesolicitud',
    templateUrl: 'detallesolicitud.html',
})
export class DetallesolicitudPage {

    @ViewChild('textAreaSolicitud') textAreaSolicitud: TextInput;

    public servicio: any;
    public formulario: FormGroup;
    public fechaActual: any = {};
    private datosGeolocalizacion: any = {};
    private pinSeleccionado: boolean = false;
    private direccionCompleta: string = "";

    constructor(private params: NavParams, public navCtrl: NavController, private fb: FormBuilder, private modal: ModalController, private storage: Storage, private geolocalizacion: Geolocation) {
        this.formulario = this.fb.group({
            descripcion: ['', Validators.required],
            fechaSolicitud: ['', Validators.required],
            direccion: ['', Validators.required],
            horaSolicitud: ['', Validators.required]
        });
    }

    ionViewDidLoad() {
        this.geolocalizacion.getCurrentPosition().then(posicion => {
            this.datosGeolocalizacion.latitud = posicion.coords.latitude;
            this.datosGeolocalizacion.longitud = posicion.coords.longitude;
            this.pinSeleccionado = true;
            console.log("coordenadas obtenidas-> ", posicion.coords);
        }).catch(err => {
            console.error(err);
        });
    }

    ionViewWillEnter() {
        this.servicio = this.params.get('servicio');
        let date = new Date();
        this.fechaActual.anioMinimo = date.getFullYear();
        this.fechaActual.anioMaximo = date.getFullYear() + "-12-31";
    }

    abrirModal() {
        console.log("abriendoModalMapa->> ", this.datosGeolocalizacion);
        const modalMapa: Modal = this.modal.create('MapasPage', {
            pin: this.pinSeleccionado,
            latitud: this.datosGeolocalizacion.latitud,
            longitud: this.datosGeolocalizacion.longitud,
            direccionCompleta: this.direccionCompleta
        });
        modalMapa.present();
        modalMapa.onDidDismiss((datos) => {
            console.log("coordenadasRecuperadas", datos);
            if (datos !== undefined) {
                this.datosGeolocalizacion = datos;
                this.formulario.get('direccion').setValue(datos.label);
                this.pinSeleccionado = datos.pin;
                this.direccionCompleta = datos.label;
            }
        });
    }

    resize() {
        this.textAreaSolicitud._native.nativeElement.style.height = this.textAreaSolicitud._native.nativeElement.scrollHeight + 'px';
    }

    SoloLectura() {
        return true;
    }

    solicitarServicio() {
        // this.navCtrl.push(ProveedorCercanoPage, {}, {
        //   animation: "ios-transition"
        // });
        let validaciones = new validarFormularios();
        this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
        let isValid: boolean = validaciones.isValidFormulario(this.formulario.controls);
        if (isValid) {
            let solicitudBody = new Solicitud();
            this.storage.get("datosUsuario").then(data => {
                solicitudBody.setSolicitudBody(this.formulario.value, this.datosGeolocalizacion, this.servicio, data.usuarioId);
                this.navCtrl.push(ProveedorCercanoPage, { solicitud: solicitudBody.getSolicitudBody() }, {
                    animation: "ios-transition"
                });
            });
        }
    }
}
