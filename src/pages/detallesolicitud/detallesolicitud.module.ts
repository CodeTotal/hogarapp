import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetallesolicitudPage } from './detallesolicitud';

@NgModule({
  declarations: [
    DetallesolicitudPage,
  ],
  imports: [
    IonicPageModule.forChild(DetallesolicitudPage),
  ],
})
export class DetallesolicitudPageModule {}
