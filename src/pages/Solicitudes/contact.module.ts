import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SolicitudPage } from './contact';
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    SolicitudPage,
  ],
  imports: [
    IonicPageModule.forChild(SolicitudPage),
    PipesModule
  ],
})
export class SolicitudPageModule {}
