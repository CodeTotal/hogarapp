import { Component } from '@angular/core';
import { NavController, LoadingController, NavParams, Modal, ModalController } from 'ionic-angular';
import { Storage } from '@ionic/storage';
import { ServiciosServicioGeneralProvider } from '../../providers/servicios-servicio-general/servicios-servicio-general';
// import 'rxjs/add/observable/forkJoin';
import { forkJoin } from 'rxjs/observable/forkJoin';

@Component({
  selector: 'page-contact',
  templateUrl: 'contact.html'
})

export class SolicitudPage {

  obtenerValoresCatgeoria: any[] = [];
  private loader: any;
  public sinSolicitudes: boolean = true;
  public listaSolicitudes: any[] = [];
  public tipoSolicitudCotizacion: string = "solicitudes";
  public tituloPrincipal: string = "Solicitudes";
  private usuarioSession: number;

  constructor(public navCtrl: NavController, private navParams: NavParams, private loadingCtrl: LoadingController, private modal: ModalController, private storage: Storage, private servicio: ServiciosServicioGeneralProvider) {

  }

  ionViewWillEnter() {
    this.inicializador();
  }


  inicializador() {
    console.log("Entrando a las solicitudes y cotizaciones");
    this.loading();
    this.navParams.data;
    this.storage.get("datosUsuario").then(usuario => {
      console.log("usuario desde Solicitudes ---> ", usuario);
      this.usuarioSession = usuario.rolId.rolId;
      if (usuario.rolId.rolId == 1 && usuario.rolId.descripcion.toUpperCase() == "CLIENTE") {
        this.solicitudesCliente(usuario.usuarioId);
      } else {
        this.tipoSolicitudCotizacion = "notificación";
        this.tituloPrincipal = "Buzón";
        console.log("Entra como proveedor y ejecuta");
        this.buscarSolicitudesPorEstado(usuario.usuarioId);
      }
    }).catch(err => console.log(err));
  }


  private solicitudesCliente(usuario: number) {
    this.servicio.getDataResource("obtenerSolicitudesDelCliente", "/" + usuario).subscribe(datos => {
      console.log("Datos del cliente solicitudes -> ", datos);
      if (datos.status == 200 && datos.ok) {
        let datosObtenidos: any = datos.body;
        let arregloDatos: any[] = datosObtenidos;
        if (arregloDatos.length >= 1) {
          this.sinSolicitudes = false;
          this.listaSolicitudes = arregloDatos;
        }
        this.loader.dismiss();
      }
    }, err => {
      console.error(err);
      this.loader.dismiss();
    });
  }


  private buscarSolicitudesPorEstado(usuario: number) {
    console.log("Ejecutando proveedor");
    let parametros = "?estado=ENVIADA&idProveedor=" + usuario;
    this.servicio.getDataResource("obtenerNotificacionesPorEstadoYIdProveedor", parametros).subscribe(datos => {
      if (datos.status == 200 && datos.ok) {
        let datosObtenidos: any = datos.body;
        let arreglodatos: any[] = datosObtenidos;
        console.log("busquedaSolicitudes por estado: ", arreglodatos);
        if (arreglodatos.length >= 1) this.listarSolicitudesPorEstado(arreglodatos);
        else this.loader.dismiss();
      }
    }, err => {
      console.log(err);
      this.loader.dismiss();
    });
  }

  private listarSolicitudesPorEstado(datos: any[]) {
    let contenedoresIdSolicitud: any[] = [];
    for (let index = 0; index < datos.length; index++) {
      let idSolicitud = datos[index].notificacionProveedorPK.solicitudId;
      contenedoresIdSolicitud.push(this.servicio.getMasivo("obtenerSolicitudPorId", "/" + idSolicitud));
    }
    forkJoin(contenedoresIdSolicitud).subscribe(enviarMasivo => {
      console.log("Enviando Masivo ---> ", enviarMasivo);
      if (enviarMasivo !== null && enviarMasivo !== undefined) {
        this.sinSolicitudes = false;
        this.listaSolicitudes = enviarMasivo;
      }
      this.loader.dismiss();
    }, err => {
      console.log(err);
    });
  }


  loading() {
    this.loader = this.loadingCtrl.create({
      content: "Espera..."
    });
    this.loader.present();
  }


  abrirDetalleSolicitudCreada(indice: number) {
    const modalMapa: Modal = this.modal.create('DetalleServicioCreadoPage', { modalServicioDetail: this.listaSolicitudes[indice], usuarioSession: this.usuarioSession });
    modalMapa.present();
    modalMapa.onDidDismiss(() => {
      this.inicializador();
    });
  }

}
