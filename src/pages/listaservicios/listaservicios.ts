import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController } from 'ionic-angular';
import { DetallesolicitudPage } from "../detallesolicitud/detallesolicitud";


@IonicPage()
@Component({
  selector: 'page-listaservicios',
  templateUrl: 'listaservicios.html',
})
export class ListaserviciosPage {

  public parametros: any[] = [];

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private modal: ModalController
  ) {

  }

  ionViewWillEnter() {
    this.parametros = this.navParams.get('servicios');
    console.log("Estos son los parametros", this.parametros);
  }

  abrirModal(detalle: string) {
    const modalDetalle = this.modal.create("DetalleservicioPage", { descripcion: detalle });
    modalDetalle.present();
  }

  solicitarServicio(indice: number) {
    this.navCtrl.push(DetallesolicitudPage, { servicio: this.parametros[indice] }, {
      animation: "ios-transition"
    });
  }

}
