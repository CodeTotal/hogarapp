import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ListaserviciosPage } from './listaservicios';

@NgModule({
  declarations: [
    ListaserviciosPage,
  ],
  imports: [
    IonicPageModule.forChild(ListaserviciosPage),
  ],
})
export class ListaserviciosPageModule {}
