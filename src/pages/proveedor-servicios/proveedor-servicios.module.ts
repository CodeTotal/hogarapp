import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProveedorServiciosPage } from './proveedor-servicios';

@NgModule({
  declarations: [
    ProveedorServiciosPage,
  ],
  imports: [
    IonicPageModule.forChild(ProveedorServiciosPage),
  ],
})
export class ProveedorServiciosPageModule {}
