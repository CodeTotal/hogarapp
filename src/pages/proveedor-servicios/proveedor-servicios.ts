import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { ServiciosServicioGeneralProvider } from "../../providers/servicios-servicio-general/servicios-servicio-general";

@IonicPage()
@Component({
  selector: 'page-proveedor-servicios',
  templateUrl: 'proveedor-servicios.html',
})
export class ProveedorServiciosPage {

  public listaServicios: any[] = [];
  private identificadoresSeervicio: any = {
    cantidadServicios: "",
    idServicios: []
  };
  public botonGuardarServicio: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private servicio: ServiciosServicioGeneralProvider) {
  }


  ionViewWillEnter() {
    let parametros = this.navParams.get("servicios");
    this.identificadoresSeervicio.idServicios = parametros;
    this.servicio.getDataResource("/obtenerCategorias").subscribe(data => {
      let bodyResponse: any = data.body;
      this.listaServicios = this.asignarChecked(bodyResponse, this.identificadoresSeervicio.idServicios);
    }, err => console.error(err));
  }

  private asignarChecked(bodyResponse: any, parametros: number[]): any {
    let respuesta: any[] = bodyResponse;
    respuesta.forEach(elemento => {
      elemento.servicioList.forEach(servicio => {
        if (parametros.length > 0) {
          this.botonGuardarServicio = true;
          let validacion = parametros.some(data => data == servicio.servicioId);
          if (validacion) servicio.checked = true;
          else servicio.checked = false;
        } else { servicio.checked = false; }
      });
    });
    return respuesta;
  }

  guardarServicios() {
    this.cerrarModal();
  }

  cerrarModal() {
    this.identificadoresSeervicio.cantidadServicios = this.identificadoresSeervicio.idServicios.length;
    this.view.dismiss(this.identificadoresSeervicio);
  }

  cambioEstadoCheckBox(evento: any) {
    let existeServicio = this.identificadoresSeervicio.idServicios.findIndex(data => data == evento);
    if (existeServicio !== -1) this.identificadoresSeervicio.idServicios.splice(existeServicio, 1);
    else this.identificadoresSeervicio.idServicios.push(+evento);
    if (this.identificadoresSeervicio.idServicios.length > 0) this.botonGuardarServicio = true;
    else this.botonGuardarServicio = false;
  }
}
