import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-detalleservicio',
  templateUrl: 'detalleservicio.html',
})
export class DetalleservicioPage {

  public descripcionServicio: string = "";

  constructor(public navParams: NavParams, private view: ViewController) {
  }

  ionViewWillEnter() {
    this.descripcionServicio = this.navParams.get('descripcion');
  }

  cerrarModal() {
    this.view.dismiss();
  }

}
