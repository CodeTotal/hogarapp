import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DetalleServicioCreadoPage } from './detalle-servicio-creado';
import { PipesModule } from "../../pipes/pipes.module";

@NgModule({
  declarations: [
    DetalleServicioCreadoPage,
  ],
  imports: [
    IonicPageModule.forChild(DetalleServicioCreadoPage),
    PipesModule
  ],
})
export class DetalleServicioCreadoPageModule {}
