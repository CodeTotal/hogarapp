import { Cotizacion } from './../../clases/cotizacion';
import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController, LoadingController, ToastController } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { validarFormularios } from '../../clases/validarFormulario';
import { Storage } from '@ionic/storage';
import { ServiciosServicioGeneralProvider } from '../../providers/servicios-servicio-general/servicios-servicio-general';


@IonicPage()
@Component({
  selector: 'page-detalle-servicio-creado',
  templateUrl: 'detalle-servicio-creado.html',
})

export class DetalleServicioCreadoPage {

  public listaCotizaciones: any[] = [];
  public formulario: FormGroup;
  public detalleServicio: any = {
    descripcion: "",
    nombre: "",
    fechaSolicitud: "",
    direccion: "",
    servicioId: null,
    cotizacionList: []
  };
  public mostrarAccionesCliente: boolean = true;
  public mostrarbadge: boolean = true;
  public numeroSolicitudes: number = 0;
  public mensajeCotizacionesVacias: boolean = false;
  public tituloCotizaciones: string = "Cotizaciones recibidas";
  public nombreServicio: string = "";
  private detalleServicioCompleto: any = {};
  public loader: any;
  private datosSession: any = {
    solicitudId: null,
    usuarioIdProveedor: null
  };
  public mostrarBotones: boolean = true;
  private usuarioSession: number;


  constructor(public navCtrl: NavController, public navParams: NavParams, private view: ViewController, private fb: FormBuilder, private storage: Storage, private servicio: ServiciosServicioGeneralProvider, private loadingCtrl: LoadingController, private toast: ToastController) {
    this.formulario = this.fb.group({
      costoServicio: ['', Validators.required],
      fechaSolicitud: ['', Validators.required],
      horaSolicitud: ['', Validators.required],
      duracion: ['', Validators.required]
    });
  }

  ionViewDidLoad() {
    console.log('detalle solicitud cliente creada');
    this.detalleServicioCompleto = this.navParams.get("modalServicioDetail");
    this.usuarioSession = this.navParams.get("usuarioSession");
    this.detalleServicio.nombre = this.detalleServicioCompleto.servicioId.nombre;
    this.detalleServicio.descripcion = this.detalleServicioCompleto.descripcion;
    this.detalleServicio.fechaSolicitud = this.detalleServicioCompleto.fechaSolicitud;
    this.detalleServicio.direccion = this.detalleServicioCompleto.direccion;
    this.detalleServicio.servicioId = this.detalleServicioCompleto.servicioId.servicioId;
    this.detalleServicio.cotizacionList = this.detalleServicioCompleto.cotizacionList;
    // this.storage.get("datosUsuario").then(usuarioSession => {
    //   this.datosSession.usuarioIdProveedor = usuarioSession.usuarioId;
    // }).catch(err => console.error(err));
    console.log("detalleDelServicio --> ", this.detalleServicio);
    console.log("DetalleServicioCompleto --> ", this.detalleServicioCompleto)
    this.renderizarPaginaPorUsuario(this.detalleServicio.cotizacionList);
  }


  private renderizarPaginaPorUsuario(cotizaciones: any[]) {
    this.storage.get("datosUsuario").then(datos => {
      this.datosSession.usuarioIdProveedor = datos.usuarioId;
      if (datos !== null && datos !== undefined) {
        if (datos.rolId.rolId == 1 || datos.rolId.descripcion.toUpperCase() == "CLIENTE") {
          // this.buscarCotizaciones();
          this.listadoCotizaciones(cotizaciones);
        } else {
          this.mostrarAccionesCliente = false;
          this.mostrarbadge = false;
          this.tituloCotizaciones = "Realizar Cotización";
        }
      }
    }).catch(err => console.error(err));
  }


  private listadoCotizaciones(cotizaciones: any[]) {
    console.log("Listado de cotizaciones --> ", cotizaciones);
    if (this.detalleServicioCompleto.estado.toUpperCase() == "ACEPTADA") this.mostrarBotones = false;
    if (cotizaciones.length >= 1) {
      this.numeroSolicitudes = cotizaciones.length;
      this.listaCotizaciones = cotizaciones.filter(data => data.estado.toUpperCase() == "CREADA");
      this.mensajeCotizacionesVacias = false;
    } else {
      this.mensajeCotizacionesVacias = true;
    }
  }

  aceptarCotizacion(index: number) {
    this.loading();
    let body = {
      cotizacionId: this.detalleServicioCompleto.cotizacionList[index].cotizacionId,
      solicitudId: {
        solicitudId: this.detalleServicioCompleto.solicitudId
      }
    };
    this.servicio.updateDataResourceSinId(body, "aceptarCotizacion").subscribe(enviado => {
      console.log("cotización aceptada ---> ", enviado);
      this.crearToast("Cotización aceptada satisfactoriamente");
      this.loader.dismiss();
      this.consultarCotizacionesPorServicio(this.detalleServicioCompleto.solicitudId);
    }, err => {
      console.error(err);
      this.loader.dismiss();
      this.crearToast("Error ocurrido al momento de aceptar una cotización");
    });
  }


  private consultarCotizacionesPorServicio(solicitudId: number) {
    if (this.usuarioSession == 1) {
      this.servicio.getDataResource("obtenerCotizacionesPorSolicitud", "/" + solicitudId).subscribe(datos => {
        if (datos.status == 200 && datos.ok) {
          let datosObtenidos: any = datos.body;
          let arreglodatos: any[] = datosObtenidos;
          if (datosObtenidos !== null && datosObtenidos !== undefined) {
            console.log("Cotizaciones por solicitud---> ", datos);
            if (arreglodatos[0].estado.toUpperCase() == "ACEPTADA") this.mostrarBotones = false;
          }
        }
      }, err => {
        console.error(err);
      });
    }
  }

  rechazarCotizacion(index: number) {
    this.loading();
    let body = {
      cotizacionId: this.detalleServicioCompleto.cotizacionList[index].cotizacionId
    };
    this.servicio.updateDataResourceSinId(body, "rechazarCotizacion").subscribe(enviado => {
      console.log("cotización rechazada ---> ", enviado);
      this.crearToast("Cotización rechazada satisfactoriamente");
      this.loader.dismiss();
      // this.consultarCotizacionesPorServicio(this.detalleServicioCompleto.solicitudId);
      this.eliminarCotizacionesRechazadas(index);
    }, err => {
      console.error(err);
      this.loader.dismiss();
      this.crearToast("Error ocurrido al momento de reachazar una cotización");
    })
  }

  private eliminarCotizacionesRechazadas(index: number) {
    this.listaCotizaciones.splice(index, 1);
    console.log("Cotizaciones rechazadas eliminadas");
    this.numeroSolicitudes = this.listaCotizaciones.length;
  }


  private crearToast(mensaje: string) {
    const toast = this.toast.create({
      message: mensaje,
      duration: 3000,
      position: 'top'
    });
    toast.present();
  }

  cerrarModal() {
    this.view.dismiss();
  }

  loading() {
    this.loader = this.loadingCtrl.create({
      content: "Espera..."
    });
    this.loader.present();
  }


  cotizarServicio() {
    this.loading();
    let cotizacion = new Cotizacion();
    if (this.validarCostoInput()) {
      let camposFormulario = this.formulario.value;
      this.datosSession.solicitudId = this.detalleServicioCompleto.solicitudId;
      cotizacion.setCotizacionBody(camposFormulario, this.datosSession);
      let bodyCotizacion = cotizacion.getCotizacionBody();
      console.log("Enviando solicitud ---> ", bodyCotizacion);
      this.servicio.sendDataResource(bodyCotizacion, "crearCotizacion").subscribe(data => {
        console.log("Cotizacion creada --> ", data);
        this.loader.dismiss();
        const toast = this.toast.create({
          message: '¡Se ha creado la cotización satisfactoriamente!',
          duration: 3000,
          position: 'top'
        });
        toast.present();
        this.view.dismiss();
      }, err => {
        console.error(err);
        this.loader.dismiss();
      });
    } else this.loader.dismiss();
  }


  private validarCostoInput(): boolean {
    let validaciones = new validarFormularios();
    this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
    let isValid: boolean = validaciones.isValidFormulario(this.formulario.controls);
    return isValid;
  }

}
