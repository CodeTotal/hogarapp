import { Component, ViewChild } from '@angular/core';
import { IonicPage, ViewController, TextInput, NavParams, LoadingController } from 'ionic-angular';

declare var $: any;
declare var H: any;
declare var mapsjs: any;


@IonicPage()
@Component({
  selector: 'page-mapas',
  templateUrl: 'mapas.html',
})
export class MapasPage {

  @ViewChild('searchAdress') searchAdress: TextInput;
  public direccionesObtenidas: any[] = [];
  private platform: any;
  private defaultLayers: any;
  private geocoder: any;
  private map: any;
  private behavior: any;
  private ui: any;
  private group: any;
  public latitud: string = "";
  public longitud: string = "";
  private objetoMapeo: any = {};
  private loader: any;

  constructor(private view: ViewController, private navParams: NavParams, private loadingCtrl: LoadingController) {
  }

  loading() {
    this.loader = this.loadingCtrl.create({
      content: "Espera..."
    });
    this.loader.present();
  }

  ionViewDidLoad() {
    console.log('Cargando mapa...');
    // this.objetoMapa();
    this.loading();
    let mapaDatosCargados: any = {
      pinSeleccionado: this.navParams.get("pin"),
      direccionCompleta: this.navParams.get("direccionCompleta"),
      latitud: this.navParams.get("latitud"),
      longitud: this.navParams.get("longitud"),
    };
    this.objetoMapeo = mapaDatosCargados;
    this.construirMapa();
  }

  cerrarModal() {
    this.view.dismiss();
  }

  enviarResponseMapa() {
    let suggestions: any[] = $('#enviarResponseMapa').data('valores');
    let filtro: any[] = suggestions.filter(data => {
      if (data.address.county == "Bogotá, D.C.") {
        let valorStreet: any[] = data.label.split(",");
        data.address.street = valorStreet[valorStreet.length - 1];
        return data;
      }
    });
    this.direccionesObtenidas = filtro;
  }

  private responseAdressComplete() {
    let datos: any = this;
    $('#enviarResponseMapa').data('valores', datos.response.suggestions);
    $('#enviarResponseMapa').click();
  }

  private responseAdressCompleteFailed() {
    console.log("Error encontrado en la obtención de los valores mapeados");
  }

  obtenerCoordenadas() {
    console.log("construye las coordenadas-->> ");
    this.latitud = $('#coordenadas').data('latitud');
    this.longitud = $('#coordenadas').data('longitud');
  }

  guardarCoordenadas() {
    let datos = {
      latitud: this.latitud,
      longitud: this.longitud,
      label: $('#coordenadas').data('label'),
      pin: this.objetoMapeo.pinSeleccionado
    };
    this.view.dismiss(datos);
  }

  buscarDireccion(evento: any) {
    var event = evento,
      textBox = this.searchAdress._native.nativeElement;
    console.log(event);

    var AUTOCOMPLETION_URL = 'https://autocomplete.geocoder.api.here.com/6.2/suggest.json',
      ajaxRequest = new XMLHttpRequest(),
      query = '';

    ajaxRequest.addEventListener("load", this.responseAdressComplete);
    ajaxRequest.addEventListener("error", this.responseAdressCompleteFailed);
    ajaxRequest.responseType = "json";

    function autoCompleteListener(textBox) {
      if (query != textBox.value) {
        if (textBox.value.length >= 1) {
          var params = '?' +
            'query=' + encodeURIComponent(textBox.value) +   // The search text which is the basis of the query
            '&country=' + 'COL' +
            '&beginHighlight=' + encodeURIComponent('') + //  Mark the beginning of the match in a token. 
            '&endHighlight=' + encodeURIComponent('') + //  Mark the end of the match in a token. 
            '&maxresults=5' +  // The upper limit the for number of suggestions to be included
            '&app_id=' + 'nhOznJTavPjLeA0U3dGS' +
            '&app_code=' + 'qBiDEC4S0UqZPi7Ou5X1UQ';
          ajaxRequest.open('GET', AUTOCOMPLETION_URL + params);
          ajaxRequest.send();
        }
      }
      query = textBox.value;
    }
    autoCompleteListener(textBox);
  }

  manejadorDragableMaker() {
    let dragableMaker = $('#manejadorDragableMaker').data();
    var mapa = dragableMaker.mapa;
    var behavior = dragableMaker.behavior;
    var platform = dragableMaker.platform;
    var group = dragableMaker.group;

    mapa.addEventListener('dragstart', function (ev) {
      var target = ev.target;
      if (target instanceof H.map.Marker) {
        behavior.disable();
      }
    }, false);

    mapa.addEventListener('dragend', function (ev) {
      var target = ev.target;
      if (target instanceof mapsjs.map.Marker) {
        behavior.enable();
      }
      var coordenada = mapa.screenToGeo(ev.currentPointer.viewportX, ev.currentPointer.viewportY);
      $('#reverseGeocodeAddress').data({
        platform: platform,
        coordenada: coordenada
      });
      $('#reverseGeocodeAddress').click();
    }, false);

    mapa.addEventListener('drag', function (ev) {
      var target = ev.target,
        pointer = ev.currentPointer;
      if (target instanceof mapsjs.map.Marker) {
        target.setPosition(mapa.screenToGeo(pointer.viewportX, pointer.viewportY));
      }
    }, false);

    mapa.setViewBounds(group.getBounds(), true);
    if (group.getObjects().length < 2) mapa.setZoom(15);
  }

  reverseGeocodeAddress() {
    let reverseGeocode = $('#reverseGeocodeAddress').data();
    var platform = reverseGeocode.platform;
    var coordenada = reverseGeocode.coordenada;

    var geocoder = platform.getGeocodingService(),
      parameters = {
        prox: coordenada.lat + "," + coordenada.lng + ",250",
        mode: 'retrieveAddresses',
        maxresults: '1',
        gen: '9'
      };

    geocoder.reverseGeocode(parameters,
      function (resultado) {
        console.log("Este es el resultado de la búsqueda", resultado);
        var data = resultado.Response.View[0].Result[0].Location.Address;
        var label = data.Label.split(",")[0] + " - " + data.District;
        $('#coordenadas').data({
          'latitud': coordenada.lat,
          'longitud': coordenada.lng,
          'label': label
        });
        $('#coordenadas').click();
      }, function (error) {
        console.log(error);
      });
  }

  limpiarMarcadoresMapa() {
    let group = $('#limpiarMarcadoresMapa').data('group');
    group.removeAll();
  }

  crearMarcadorMapa(data: any) {
    $('#limpiarMarcadoresMapa').data({ group: this.group });
    $('#limpiarMarcadoresMapa').click();
    this.objetoMapeo.pinSeleccionado = true;
    var imagenUsuario = document.getElementById('imagenIconoUsuario');
    var mapa = this.map;
    var group = this.group;
    var behavior = this.behavior;
    var platform = this.platform;
    this.direccionesObtenidas = [];
    this.searchAdress._native.nativeElement.value = "";

    var geocodingParameters = {
      locationId: data.locationId
    },
      onResult = function (resultado) {
        console.log("ResultadoObtenidoGeocoder", resultado);
        var locations = resultado.Response.View[0].Result;
        var latitud = locations[0].Location.DisplayPosition.Latitude;
        var longitud = locations[0].Location.DisplayPosition.Longitude;
        var direccionCompleta = data.address.street + ", " + data.address.district + ", " + data.address.county;
        $('#coordenadas').data({
          'latitud': latitud,
          'longitud': longitud,
          'label': direccionCompleta
        });
        var marker = new H.map.Marker({
          lat: latitud,
          lng: longitud
        }, { icon: new H.map.Icon(imagenUsuario) });
        marker.draggable = true; //permitir el desplazamiento del marcador en el mapa
        group.addObject(marker);

        $('#manejadorDragableMaker').data({
          mapa: mapa,
          group: group,
          behavior: behavior,
          platform: platform
        });
        $('#manejadorDragableMaker').click();
        $('#coordenadas').click();
      },
      onError = function (error) {
        console.log("errorGenerado", error);
      };
    this.geocoder.geocode(geocodingParameters, onResult, onError);
  }

  private construirMapa() {
    this.platform = new H.service.Platform({
      app_id: 'nhOznJTavPjLeA0U3dGS',
      app_code: 'qBiDEC4S0UqZPi7Ou5X1UQ',
      // useCIT: false,
      useHTTPS: true
    });

    this.defaultLayers = this.platform.createDefaultLayers({
      tileSize: 512,
      ppi: 320
    });
    this.geocoder = this.platform.getGeocodingService();
    this.group = new H.map.Group();

    this.map = new H.Map(document.getElementById('map'),
      this.defaultLayers.normal.map, {
        // center: { lat: 4.6022608, lng: -74.0897502 },
        center: { lat: 4.6022608, lng: -74.0897502 },
        zoom: 11
        // , pixelRatio: 2
      });

    this.map.addObject(this.group);

    this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));
    this.ui = H.ui.UI.createDefault(this.map, this.defaultLayers);
    console.log("behavior", this.behavior);
    console.log(this.ui);

    // Decisión---> mostrar el pin si anteriormente se había seleccionado en el mapa
    if (this.objetoMapeo.pinSeleccionado) {
      this.pintarMaracadorInicial();
      console.log("Entra-> mapa cargado anteriormente");
    }
    this.loader.dismiss();
  }

  private pintarMaracadorInicial() {
    var mapa = this.map;
    var group = this.group;
    var behavior = this.behavior;
    var platform = this.platform;
    var mapeo = this.objetoMapeo;
    var imagenUsuario = document.getElementById('imagenIconoUsuario');
    var coordenada = {
      lat: mapeo.latitud,
      lng: mapeo.longitud
    };
    if (mapeo.direccionCompleta == "") {
      $('#reverseGeocodeAddress').data({
        platform: platform,
        coordenada: coordenada
      });
      $('#reverseGeocodeAddress').click();
    }
    $('#coordenadas').data({
      'latitud': mapeo.latitud,
      'longitud': mapeo.longitud,
      'label': mapeo.direccionCompleta
    });
    var marker = new H.map.Marker({
      lat: mapeo.latitud,
      lng: mapeo.longitud
    }, { icon: new H.map.Icon(imagenUsuario) });
    marker.draggable = true; //permitir el desplazamiento del marcador en el mapa
    group.addObject(marker);

    $('#manejadorDragableMaker').data({
      mapa: mapa,
      group: group,
      behavior: behavior,
      platform: platform
    });
    $('#manejadorDragableMaker').click();
    $('#coordenadas').click();
  }

  objetoMapa() {

    function addDraggableMarker(map, behavior) {
      var imagenUsuario = document.getElementById('imagenIconoUsuario');
      console.log("Imagen", imagenUsuario);

      var marker = new H.map.Marker({
        lat: 4.6022608,
        lng: -74.0897502
      });

      marker.draggable = true;
      map.addObject(marker);

      map.addEventListener('dragstart', function (ev) {
        var target = ev.target;
        if (target instanceof H.map.Marker) {
          behavior.disable();
        }
      }, false);

      map.addEventListener('dragend', function (ev) {
        var target = ev.target;
        console.log(target);
        if (target instanceof mapsjs.map.Marker) {
          behavior.enable();
        }
      }, false);

      map.addEventListener('drag', function (ev) {
        var target = ev.target,
          pointer = ev.currentPointer;
        if (target instanceof mapsjs.map.Marker) {
          target.setPosition(map.screenToGeo(pointer.viewportX, pointer.viewportY));
        }
      }, false);
    }




    // Configuración general del mapa
    var platform = new H.service.Platform({
      app_id: 'nhOznJTavPjLeA0U3dGS',
      app_code: 'qBiDEC4S0UqZPi7Ou5X1UQ',
      useHTTPS: true
    });
    var defaultLayers = platform.createDefaultLayers({});

    var map = new H.Map(document.getElementById('map'),
      defaultLayers.normal.map, {
        center: { lat: 4.6022608, lng: -74.0897502 },
        zoom: 12
      });

    var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
    var ui = H.ui.UI.createDefault(map, defaultLayers);
    console.log(ui);
    addDraggableMarker(map, behavior);
  }

}
