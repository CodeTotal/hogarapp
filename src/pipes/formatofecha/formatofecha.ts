import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'formatofecha',
})
export class FormatofechaPipe implements PipeTransform {

  transform(value: string, args: any[]) {
    let obtenervalorFecha = value.substring(0, value.indexOf("."));
    obtenervalorFecha = obtenervalorFecha.replace("T", " - ");
    let nuevafecha = obtenervalorFecha.split("-");
    obtenervalorFecha = nuevafecha[2] + "-" + nuevafecha[1] + "-" + nuevafecha[0] + " " + nuevafecha[3];
    return obtenervalorFecha;
  }

}
