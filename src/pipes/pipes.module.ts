import { NgModule } from '@angular/core';
import { FormatofechaPipe } from './formatofecha/formatofecha';
@NgModule({
	declarations: [FormatofechaPipe],
	imports: [],
	exports: [FormatofechaPipe]
})
export class PipesModule {}
