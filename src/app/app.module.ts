import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { ConfiguracionPage } from '../pages/Configuraciones/about';
import { SolicitudPageModule } from '../pages/Solicitudes/contact.module';
import { InicioSesionPageModule } from '../pages/inicio-sesion/inicio-sesion.module';
import { TabsPage } from '../pages/tabs/tabs';
import { RegistroUsuarioPageModule } from "../pages/registro-usuario/registro-usuario.module";
import { ListaserviciosPageModule } from "../pages/listaservicios/listaservicios.module";
import { DetallesolicitudPageModule } from "../pages/detallesolicitud/detallesolicitud.module";
import { InicioPage } from "../pages/Inicio/home";
import { ProveedorCercanoPageModule } from "../pages/proveedor-cercano/proveedor-cercano.module";
import { AdministradorPageModule } from "../pages/administrador/administrador.module";
import { DetalleServicioCreadoPageModule } from "../pages/detalle-servicio-creado/detalle-servicio-creado.module";

import { HttpClientModule } from "@angular/common/http";
import { ReactiveFormsModule } from '@angular/forms';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ServiciosServicioGeneralProvider } from '../providers/servicios-servicio-general/servicios-servicio-general';
import { IonicStorageModule } from '@ionic/storage';
import { Geolocation } from '@ionic-native/geolocation';
import { File } from '@ionic-native/file';

@NgModule({
  declarations: [
    MyApp,
    ConfiguracionPage,
    // SolicitudPage,
    TabsPage,
    InicioPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp)
    , HttpClientModule
    , ReactiveFormsModule
    , DetallesolicitudPageModule
    , InicioSesionPageModule
    , ListaserviciosPageModule
    , RegistroUsuarioPageModule
    , ProveedorCercanoPageModule
    , AdministradorPageModule
    , DetalleServicioCreadoPageModule
    , SolicitudPageModule
    , IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    ConfiguracionPage,
    // SolicitudPage,
    TabsPage,
    InicioPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ServiciosServicioGeneralProvider,
    Geolocation,
    File
  ]
})
export class AppModule {}
