import { Component } from '@angular/core';
import { Platform, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { InicioSesionPage } from "../pages/inicio-sesion/inicio-sesion";
import { TabsPage } from "../pages/tabs/tabs";
import { File } from '@ionic-native/file';
import { Storage } from '@ionic/storage';
import { ServiciosServicioGeneralProvider } from '../providers/servicios-servicio-general/servicios-servicio-general';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  rootPage: any;

  constructor(platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private file: File, private storage: Storage, private servicio: ServiciosServicioGeneralProvider, public app: App) {
    platform.ready().then(() => {
      statusBar.styleDefault();
      splashScreen.hide();
      this.entrarVistaSeleccionada();
    });
  }

  entrarVistaSeleccionada() {
    this.storage.get("logueado").then(data => {
      this.establecerEnlace(data);
    }).catch(err => {
      console.log(err);
    })
  }

  private establecerEnlace(data) {
    let ruta = this.file.dataDirectory + "configuracion";
    this.servicio.setConfiguracion("192.168.0.15:", "8092/");
    this.rootPage = InicioSesionPage;
    // this.file.readAsText(ruta, "rootHogarapp").then(leer => {
    //   let atributosConexion = leer.split("|");
    //   this.servicio.setConfiguracion(atributosConexion[0], atributosConexion[1]);
    //   if (data == null) this.rootPage = InicioSesionPage;
    //   else this.rootPage = TabsPage;
    // }).catch(err => {
    //   console.log(JSON.stringify(err));
    //   this.establecerConfiguracionApp();
    // });
  }

  private establecerConfiguracionApp() {
    let ruta = this.file.dataDirectory + "configuracion";
    let texto = "192.168.0.15:|8092/";
    this.file.checkDir(this.file.dataDirectory, 'configuracion').then(existencia => { }).catch(err => {
      this.file.createDir(this.file.dataDirectory, 'configuracion', true).then(directorio => {
        this.file.checkDir(this.file.dataDirectory, 'configuracion').then(existencia => {
          this.file.createFile(ruta, "rootHogarapp", true).then(crear => {
            this.file.writeFile(this.file.dataDirectory + "configuracion", "rootHogarapp", texto, { replace: true }).then(escribir => {
              // se recarga de nuevo la página automáticamente
              this.app.getRootNavs()[0].push(InicioSesionPage);
            }).catch(err => { });
          }).catch(err => { });
        }).catch(err => { });
      }).catch(err => { });
    });
  }

}
