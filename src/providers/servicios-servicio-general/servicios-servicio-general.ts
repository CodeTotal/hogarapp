import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/timeout';


@Injectable()
export class ServiciosServicioGeneralProvider {

  private protocolo: string = "http://";
  // private hots: string = "192.168.0.14:"
  // private puerto: string = "8092/";
  private hots: string;
  private puerto: string;
  private aplicacion: string = "";
  private enlaceGenerico: string;

  constructor(public http: HttpClient) {
    console.log('Llamando servicio...');
  }

  public getDataResource(enlace: string, id: string = "") {
    return this.http.get(this.enlaceGenerico + enlace + id, { observe: 'response' }).timeout(10000);
  }

  public getMasivo(enlace: string, id: string = ""){
    return this.http.get(this.enlaceGenerico + enlace + id);
  }

  sendDataResource(body: any, enlace: string = "") {
    let cabecera = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.post(this.enlaceGenerico + enlace, body, { headers: cabecera });
  }

  updateDataResource(id: number, body: any, cadena: string = "") {
    let cabecera = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put((this.enlaceGenerico + cadena + "/" + id), body, { headers: cabecera });
  }

  updateDataResourceSinId(body: any, cadena: string = "") {
    let cabecera = new HttpHeaders().set('Content-Type', 'application/json; charset=utf-8');
    return this.http.put((this.enlaceGenerico + cadena), body, { headers: cabecera });
  }

  deleteDataResource(enlace: string, id: number) {
    return this.http.delete((this.enlaceGenerico + enlace + "/" + id));
  }

  setConfiguracion(host: string, puerto: string) {
    this.hots = host;
    this.puerto = puerto;
    this.enlaceGenerico = this.protocolo + this.hots + this.puerto;
  }

  getConfiguracion() {
    return this.enlaceGenerico;
  }

}
