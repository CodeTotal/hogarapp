webpackJsonp([3],{

/***/ 118:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 118;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/administrador/administrador.module": [
		162
	],
	"../pages/detalle-servicio-creado/detalle-servicio-creado.module": [
		174
	],
	"../pages/detalleservicio/detalleservicio.module": [
		314,
		2
	],
	"../pages/detallesolicitud/detallesolicitud.module": [
		176
	],
	"../pages/inicio-sesion/inicio-sesion.module": [
		177
	],
	"../pages/listaservicios/listaservicios.module": [
		178
	],
	"../pages/mapas/mapas.module": [
		315,
		1
	],
	"../pages/proveedor-cercano/proveedor-cercano.module": [
		179
	],
	"../pages/proveedor-servicios/proveedor-servicios.module": [
		316,
		0
	],
	"../pages/registro-usuario/registro-usuario.module": [
		180
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return Promise.all(ids.slice(1).map(__webpack_require__.e)).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 161;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdministradorPageModule", function() { return AdministradorPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__administrador__ = __webpack_require__(163);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var AdministradorPageModule = /** @class */ (function () {
    function AdministradorPageModule() {
    }
    AdministradorPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__administrador__["a" /* AdministradorPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__administrador__["a" /* AdministradorPage */]),
            ],
        })
    ], AdministradorPageModule);
    return AdministradorPageModule;
}());

//# sourceMappingURL=administrador.module.js.map

/***/ }),

/***/ 163:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AdministradorPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__clases_validarFormulario__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__inicio_sesion_inicio_sesion__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var AdministradorPage = /** @class */ (function () {
    function AdministradorPage(navCtrl, navParams, fb, loadingCtrl, alert, file) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.loadingCtrl = loadingCtrl;
        this.alert = alert;
        this.file = file;
        this.formulario = this.fb.group({
            ip: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            puerto: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
    }
    AdministradorPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('Página de administración cargada');
        this.ruta = this.file.dataDirectory + "configuracion";
        this.file.readAsText(this.ruta, "rootHogarapp").then(function (leer) {
            var atributosConexion = leer.split("|");
            _this.formulario.get("ip").setValue(atributosConexion[0].replace(":", ""));
            _this.formulario.get("puerto").setValue(atributosConexion[1].replace("/", ""));
        }).catch(function (err) {
            alert(err);
        });
    };
    AdministradorPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    AdministradorPage.prototype.regresarInicioSesion = function () {
        this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */], {}, { animate: true });
    };
    AdministradorPage.prototype.asignarConfiguacion = function () {
        var _this = this;
        var validaciones = new __WEBPACK_IMPORTED_MODULE_3__clases_validarFormulario__["a" /* validarFormularios */]();
        this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
        var isValid = validaciones.isValidFormulario(this.formulario.controls);
        if (isValid) {
            this.loading();
            console.log("verificacion de los campos --> verdadera");
            var datosConfiguracion = this.formulario.get("ip").value + ":|" + this.formulario.get("puerto").value + "/";
            this.file.writeFile(this.ruta, "rootHogarapp", datosConfiguracion, { replace: true }).then(function (escritura) {
                console.log("archivo escrito" + escritura);
                _this.loader.dismiss();
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */]);
            }).catch(function (err) {
                console.log(err);
                _this.loader.dismiss();
                validaciones.crearAlerta(_this.alert, "error", "Error creando el archivo de configuación");
            });
        }
    };
    AdministradorPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-administrador',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\administrador\administrador.html"*/'<ion-content padding class="fondoConfiguracion">\n  <ion-card class="sombreadoTarjeta posicionCentradaTarjeta" [formGroup]="formulario">\n    <ion-card-header class="colores">\n      Configuración\n    </ion-card-header>\n    <ion-card-content>\n      <p class="subtitulo">Configuración del puerto y la dirección IP</p>\n      <br>\n      <ion-list class="formulario">\n\n        <ion-item class="separado">\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'ip\'].invalid && (formulario.controls[\'ip\'].dirty || formulario.controls[\'ip\'].touched))}" maxlength="40" formControlName="ip" class="usuarioInput" type="text" placeholder="Dirección IP"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'ip\'].invalid && (formulario.controls[\'ip\'].dirty || formulario.controls[\'ip\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n\n        <ion-item class="separado">\n          <ion-icon name="heart"></ion-icon>\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'puerto\'].invalid && (formulario.controls[\'puerto\'].dirty || formulario.controls[\'puerto\'].touched))}" maxlength="40" formControlName="puerto" class="usuarioInput" type="text" placeholder="Puerto"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'puerto\'].invalid && (formulario.controls[\'puerto\'].dirty || formulario.controls[\'puerto\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n      </ion-list>\n\n      <div class="formulario separado ubicacionBotones separadoAbajo">\n          <button ion-button color="danger" (click)="regresarInicioSesion()">Regresar</button>\n          <button ion-button color="colorPrimario" (click)="asignarConfiguacion()">Asignar</button>\n      </div>\n      \n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\administrador\administrador.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */]])
    ], AdministradorPage);
    return AdministradorPage;
}());

//# sourceMappingURL=administrador.js.map

/***/ }),

/***/ 166:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return RegistroUsuarioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__tabs_tabs__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__clases_validarFormulario__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var RegistroUsuarioPage = /** @class */ (function () {
    function RegistroUsuarioPage(navCtrl, navParams, fb, modal, servicio, alert, loadingCtrl, storage, geolocalizacion) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.fb = fb;
        this.modal = modal;
        this.servicio = servicio;
        this.alert = alert;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.geolocalizacion = geolocalizacion;
        this.latitud = "";
        this.longitud = "";
        this.idServicios = [];
        this.pinSeleccionado = false;
        this.direccionCompleta = "";
        this.formulario = this.fb.group({
            descripcionPerfil: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            nombres: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            apellidos: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            correo: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].compose([
                    __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required, __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].email
                ])],
            direccion: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            telefono: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            password: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            servicios: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]
        });
    }
    RegistroUsuarioPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('Pagina de registro activada');
        this.geolocalizacion.getCurrentPosition().then(function (posicion) {
            _this.latitud = "" + posicion.coords.latitude;
            _this.longitud = "" + posicion.coords.longitude;
            _this.pinSeleccionado = true;
            console.log("coordenadas obtenidas-> ", posicion.coords);
        }).catch(function (err) {
            console.error(err);
        });
    };
    RegistroUsuarioPage.prototype.regresarPaginaPrincipal = function () {
        this.navCtrl.pop();
    };
    RegistroUsuarioPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    RegistroUsuarioPage.prototype.enviarDatosPersonales = function () {
        var _this = this;
        // debugger;
        var validaciones = new __WEBPACK_IMPORTED_MODULE_4__clases_validarFormulario__["a" /* validarFormularios */]();
        var enlace = "crearProveedor";
        this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
        if (this.formulario.get('descripcionPerfil').value == 2) {
            this.formulario.controls.servicios.setValue(" ");
            enlace = "crearCliente";
        }
        var isValid = validaciones.isValidFormulario(this.formulario.controls);
        if (isValid) {
            this.loading();
            var datosEnviados = this.formulario.value;
            var latitud = "" + this.latitud;
            var longitud = "" + this.longitud;
            datosEnviados.coordenadaId = {
                latitud: latitud.replace(".", ","),
                longitud: longitud.replace(".", ",")
            };
            datosEnviados.username = "";
            datosEnviados.servicesId = [];
            datosEnviados.identificacion = "12334678";
            (this.formulario.get("servicios").value == " ") ? "" : datosEnviados.servicesId = this.idServicios;
            this.servicio.sendDataResource(datosEnviados, enlace).subscribe(function (data) {
                var datos = data;
                _this.storage.clear();
                _this.storage.set("datosUsuario", datos);
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_0__tabs_tabs__["a" /* TabsPage */], { tipoUsuario: _this.formulario.get("descripcionPerfil").value });
            }, function (err) {
                console.error(err);
                var alerta = validaciones.crearAlerta(_this.alert, "Error", err.error);
                alerta.present();
                _this.loader.dismiss();
            }, function () {
                _this.loader.dismiss();
            });
        }
        else {
            console.log(this.formulario);
        }
    };
    RegistroUsuarioPage.prototype.SoloLectura = function () {
        return true;
    };
    RegistroUsuarioPage.prototype.abrirModal = function () {
        var _this = this;
        var modalMapa = this.modal.create('MapasPage', {
            pin: this.pinSeleccionado,
            latitud: this.latitud,
            longitud: this.longitud,
            direccionCompleta: this.direccionCompleta
        });
        modalMapa.present();
        modalMapa.onDidDismiss(function (datos) {
            console.log("coordenadasRecuperadas", datos);
            if (datos !== undefined) {
                _this.latitud = datos.latitud;
                _this.longitud = datos.longitud;
                _this.formulario.get('direccion').setValue(datos.label);
                _this.pinSeleccionado = datos.pin;
                _this.direccionCompleta = datos.label;
            }
        });
    };
    RegistroUsuarioPage.prototype.abrirModalServicios = function () {
        var _this = this;
        var modalMapaServicio = this.modal.create('ProveedorServiciosPage', { servicios: this.idServicios });
        modalMapaServicio.present();
        modalMapaServicio.onDidDismiss(function (datos) {
            console.log("datosRecibidos->> ", datos);
            if (datos !== undefined && datos.cantidadServicios > 0) {
                _this.formulario.get("servicios").setValue(datos.cantidadServicios + " servicios");
                _this.idServicios = datos.idServicios;
            }
            else
                _this.formulario.get("servicios").reset();
        });
    };
    RegistroUsuarioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-registro-usuario',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\registro-usuario\registro-usuario.html"*/'<ion-header>\n\n  <ion-navbar color="materialDesign">\n    <ion-title>Datos personales</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n  <ion-card class="sombreadoTarjeta">\n    <ion-card-content>\n      <p class="subtitulo">Ingresa tus datos personales para empezar a disfrutar de las bondades de HogarApp</p>\n      <br>\n      <ion-list [formGroup]="formulario">\n\n        <ion-item class="usuarioSelect" [ngClass]="{mensajeError: (formulario.controls[\'descripcionPerfil\'].invalid && (formulario.controls[\'descripcionPerfil\'].dirty || formulario.controls[\'descripcionPerfil\'].touched))}">\n          <ion-label class="labelSelect">Tipo usuario</ion-label>\n          <ion-select interface="action-sheet" cancelText="Cancelar" formControlName="descripcionPerfil">\n            <ion-option value="1">Proveedor</ion-option>\n            <ion-option value="2">Cliente</ion-option>\n          </ion-select>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'descripcionPerfil\'].invalid && (formulario.controls[\'descripcionPerfil\'].dirty || formulario.controls[\'descripcionPerfil\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n\n        <ion-item class="separadoFormulario">\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'nombres\'].invalid && (formulario.controls[\'nombres\'].dirty || formulario.controls[\'nombres\'].touched))}"\n            formControlName="nombres" class="usuarioInput" type="text" placeholder="Nombres" maxlength="40"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'nombres\'].invalid && (formulario.controls[\'nombres\'].dirty || formulario.controls[\'nombres\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n\n\n        <ion-item class="separadoFormulario">\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'apellidos\'].invalid && (formulario.controls[\'apellidos\'].dirty || formulario.controls[\'apellidos\'].touched))}"\n            formControlName="apellidos" class="usuarioInput" type="text" placeholder="Apellidos" maxlength="40"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'apellidos\'].invalid && (formulario.controls[\'apellidos\'].dirty || formulario.controls[\'apellidos\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n\n\n        <ion-item class="separadoFormulario">\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'correo\'].invalid && (formulario.controls[\'correo\'].dirty || formulario.controls[\'correo\'].touched))}"\n            formControlName="correo" class="usuarioInput" type="text" placeholder="Correo electrónico" maxlength="40"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'correo\'].invalid && (formulario.controls[\'correo\'].dirty || formulario.controls[\'correo\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido / formato inválido\n        </div>\n\n        <ion-item class="separadoFormulario">\n          <ion-input [readonly]="SoloLectura()" [ngClass]="{mensajeError: (formulario.controls[\'direccion\'].invalid && (formulario.controls[\'direccion\'].dirty || formulario.controls[\'direccion\'].touched))}"\n            formControlName="direccion" class="usuarioInput" type="text" placeholder="Dirección"></ion-input>\n        </ion-item>\n        <div>\n          <!-- Botón de la ventana modal -->\n          <button (click)="abrirModal()" class="ubicarBoton" ion-button icon-start color="colorPrimario">\n            <ion-icon name="search"></ion-icon>\n          </button>\n          <div *ngIf="formulario.controls[\'direccion\'].invalid && (formulario.controls[\'direccion\'].dirty || formulario.controls[\'direccion\'].touched)"\n            class="colorMensaje">\n            Error: Campo requerido\n          </div>\n        </div>\n\n        <div *ngIf="formulario.value.descripcionPerfil == \'1\'">\n          <ion-item class="separadoFormulario">\n            <ion-input [readonly]="SoloLectura()" [ngClass]="{mensajeError: (formulario.controls[\'servicios\'].invalid && (formulario.controls[\'servicios\'].dirty || formulario.controls[\'servicios\'].touched))}"\n              formControlName="servicios" class="usuarioInput" type="text" placeholder="Servicios"></ion-input>\n          </ion-item>\n          <div>\n            <!-- Botón de la ventana modal -->\n            <button (click)="abrirModalServicios()" class="ubicarBoton" ion-button icon-start color="colorPrimario">\n              <ion-icon name="search"></ion-icon>\n            </button>\n            <div *ngIf="formulario.controls[\'servicios\'].invalid && (formulario.controls[\'servicios\'].dirty || formulario.controls[\'servicios\'].touched)"\n              class="colorMensaje">\n              Error: Campo requerido\n            </div>\n          </div>\n        </div>\n\n\n        <ion-item class="separadoFormulario">\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'telefono\'].invalid && (formulario.controls[\'telefono\'].dirty || formulario.controls[\'telefono\'].touched))}"\n            formControlName="telefono" class="usuarioInput" type="number" placeholder="Celular" maxlength="10" minlength="10"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'telefono\'].invalid && (formulario.controls[\'telefono\'].dirty || formulario.controls[\'telefono\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido / longitud(10 dígitos)\n        </div>\n\n        <ion-item class="separadoFormulario">\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'password\'].invalid && (formulario.controls[\'password\'].dirty || formulario.controls[\'password\'].touched))}"\n            formControlName="password" class="usuarioInput" type="password" placeholder="Contraseña"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'password\'].invalid && (formulario.controls[\'password\'].dirty || formulario.controls[\'password\'].touched)"\n          class="colorMensaje" maxlength="15">\n          Error: Campo requerido\n        </div>\n      </ion-list>\n\n\n      <div class="separado ubicacionBotones separadoAbajo">\n        <button ion-button color="danger" (click)="regresarPaginaPrincipal()">Cancelar</button>\n        <button ion-button color="colorPrimario" (click)="enviarDatosPersonales()">Registrar</button>\n      </div>\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\registro-usuario\registro-usuario.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */],
            __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */]])
    ], RegistroUsuarioPage);
    return RegistroUsuarioPage;
}());

//# sourceMappingURL=registro-usuario.js.map

/***/ }),

/***/ 167:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ConfiguracionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__inicio_sesion_inicio_sesion__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(22);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConfiguracionPage = /** @class */ (function () {
    function ConfiguracionPage(navCtrl, storage, loadingCtrl, app) {
        this.navCtrl = navCtrl;
        this.storage = storage;
        this.loadingCtrl = loadingCtrl;
        this.app = app;
        this.imagenUsuario = "/assets/imgs/perfilusuario3.png";
        this.datosUsuario = {
            nombres: "",
            apellidos: "",
            direccion: "",
            telefono: "",
            estado: ""
        };
    }
    ConfiguracionPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    ConfiguracionPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.storage.get("datosUsuario").then(function (datosUsuario) {
            console.log("datosUsuario -> Perfil", datosUsuario);
            if (datosUsuario !== null) {
                if (datosUsuario.rolId.rolId == 2 || datosUsuario.rolId.descripcion.toUpperCase() !== "CLIENTE")
                    _this.imagenUsuario = "/assets/imgs/perfilProveedor3.png";
                _this.datosUsuario = datosUsuario;
            }
        }).catch(function (err) { return console.error(err); });
    };
    ConfiguracionPage.prototype.cerrarSesion = function () {
        var _this = this;
        this.loading();
        this.storage.remove("datosUsuario").then(function () {
            console.log("Elimina datos del usuario al salir");
            _this.storage.remove("logueo").then(function () {
                console.log("variables locales eliminadas al salir");
                _this.loader.dismiss();
                _this.app.getRootNavs()[0].push(__WEBPACK_IMPORTED_MODULE_0__inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */]);
            });
        }).catch(function (err) { return console.error(err); });
    };
    ConfiguracionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-about',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\Configuraciones\about.html"*/'<ion-header>\n  <ion-navbar color="materialDesign">\n    <ion-title>\n      Perfil\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n  <div style="height: 30%; width: 100%; background-image: url(/assets/imgs/universe.jpg); background-attachment: fixed;">\n    <div style="text-align: center;">\n      <img style="margin-top: 100px;" [src]=imagenUsuario alt="imagen no encontrada" width=110 height=110>\n    </div>\n  </div>\n\n  <ion-list style="margin-top: 15%">\n    <ion-item>\n      <ion-icon name="contact" item-start></ion-icon>\n      {{datosUsuario.nombres}}\n    </ion-item>\n    <ion-item>\n      <ion-icon name="contact" item-start></ion-icon>\n      {{datosUsuario.apellidos}}\n    </ion-item>\n    <ion-item>\n      <ion-icon name="pin" item-start></ion-icon>\n      {{datosUsuario.direccion}}\n    </ion-item>\n    <ion-item>\n      <ion-icon name="phone-portrait" item-start></ion-icon>\n      {{datosUsuario.telefono}}\n    </ion-item>\n    <ion-item>\n      <ion-icon name="pulse" item-start></ion-icon>\n      {{datosUsuario.estado}}\n    </ion-item>\n    <button ion-item (click)="cerrarSesion()">\n      <ion-icon name="log-out" item-start></ion-icon>\n      <div style="color: red"><b>Cerrar Sesión</b></div>\n    </button>\n\n  </ion-list>\n\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\Configuraciones\about.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["b" /* App */]])
    ], ConfiguracionPage);
    return ConfiguracionPage;
}());

//# sourceMappingURL=about.js.map

/***/ }),

/***/ 168:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SolicitudPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_storage__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_forkJoin__ = __webpack_require__(131);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_rxjs_observable_forkJoin___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_forkJoin__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




// import 'rxjs/add/observable/forkJoin';

var SolicitudPage = /** @class */ (function () {
    function SolicitudPage(navCtrl, navParams, loadingCtrl, modal, storage, servicio) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.modal = modal;
        this.storage = storage;
        this.servicio = servicio;
        this.obtenerValoresCatgeoria = [];
        this.sinSolicitudes = true;
        this.listaSolicitudes = [];
        this.tipoSolicitudCotizacion = "solicitudes";
        this.tituloPrincipal = "Solicitudes";
    }
    SolicitudPage.prototype.ionViewWillEnter = function () {
        this.inicializador();
    };
    SolicitudPage.prototype.inicializador = function () {
        var _this = this;
        console.log("Entrando a las solicitudes y cotizaciones");
        this.loading();
        this.navParams.data;
        this.storage.get("datosUsuario").then(function (usuario) {
            console.log("usuario desde Solicitudes ---> ", usuario);
            _this.usuarioSession = usuario.rolId.rolId;
            if (usuario.rolId.rolId == 1 && usuario.rolId.descripcion.toUpperCase() == "CLIENTE") {
                _this.solicitudesCliente(usuario.usuarioId);
            }
            else {
                _this.tipoSolicitudCotizacion = "notificación";
                _this.tituloPrincipal = "Buzón";
                console.log("Entra como proveedor y ejecuta");
                _this.buscarSolicitudesPorEstado(usuario.usuarioId);
            }
        }).catch(function (err) { return console.log(err); });
    };
    SolicitudPage.prototype.solicitudesCliente = function (usuario) {
        var _this = this;
        this.servicio.getDataResource("obtenerSolicitudesDelCliente", "/" + usuario).subscribe(function (datos) {
            console.log("Datos del cliente solicitudes -> ", datos);
            if (datos.status == 200 && datos.ok) {
                var datosObtenidos = datos.body;
                var arregloDatos = datosObtenidos;
                if (arregloDatos.length >= 1) {
                    _this.sinSolicitudes = false;
                    _this.listaSolicitudes = arregloDatos;
                }
                _this.loader.dismiss();
            }
        }, function (err) {
            console.error(err);
            _this.loader.dismiss();
        });
    };
    SolicitudPage.prototype.buscarSolicitudesPorEstado = function (usuario) {
        var _this = this;
        console.log("Ejecutando proveedor");
        var parametros = "?estado=ENVIADA&idProveedor=" + usuario;
        this.servicio.getDataResource("obtenerNotificacionesPorEstadoYIdProveedor", parametros).subscribe(function (datos) {
            if (datos.status == 200 && datos.ok) {
                var datosObtenidos = datos.body;
                var arreglodatos = datosObtenidos;
                console.log("busquedaSolicitudes por estado: ", arreglodatos);
                if (arreglodatos.length >= 1)
                    _this.listarSolicitudesPorEstado(arreglodatos);
                else
                    _this.loader.dismiss();
            }
        }, function (err) {
            console.log(err);
            _this.loader.dismiss();
        });
    };
    SolicitudPage.prototype.listarSolicitudesPorEstado = function (datos) {
        var _this = this;
        var contenedoresIdSolicitud = [];
        for (var index = 0; index < datos.length; index++) {
            var idSolicitud = datos[index].notificacionProveedorPK.solicitudId;
            contenedoresIdSolicitud.push(this.servicio.getMasivo("obtenerSolicitudPorId", "/" + idSolicitud));
        }
        Object(__WEBPACK_IMPORTED_MODULE_4_rxjs_observable_forkJoin__["forkJoin"])(contenedoresIdSolicitud).subscribe(function (enviarMasivo) {
            console.log("Enviando Masivo ---> ", enviarMasivo);
            if (enviarMasivo !== null && enviarMasivo !== undefined) {
                _this.sinSolicitudes = false;
                _this.listaSolicitudes = enviarMasivo;
            }
            _this.loader.dismiss();
        }, function (err) {
            console.log(err);
        });
    };
    SolicitudPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    SolicitudPage.prototype.abrirDetalleSolicitudCreada = function (indice) {
        var _this = this;
        var modalMapa = this.modal.create('DetalleServicioCreadoPage', { modalServicioDetail: this.listaSolicitudes[indice], usuarioSession: this.usuarioSession });
        modalMapa.present();
        modalMapa.onDidDismiss(function () {
            _this.inicializador();
        });
    };
    SolicitudPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-contact',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\Solicitudes\contact.html"*/'<ion-header>\n  <ion-navbar color="materialDesign">\n    <ion-title>\n      {{tituloPrincipal}}\n    </ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="separado card-background-page">\n\n  <div *ngIf="sinSolicitudes" style="height: 100%; width: 100%;">\n    <img style="opacity: 0.3; display: block; margin: auto; margin-top: 10%;" src="/assets/imgs/robot.png" alt="No se encontró el recurso"\n      width=200 height=270>\n    <p style="font-size: 17px; color: rgba(129, 128, 128, 0.452); text-align: center;">No se han realizado {{tipoSolicitudCotizacion}}</p>\n  </div>\n\n  <!-- <ion-card (click)="listarServicios(indice)" class="sombreadoTarjetaCategorias separarTarjetas ajusteTarjeta" *ngFor="let item of obtenerValoresCategoria; let indice = index;"> --->\n  <div *ngIf="!sinSolicitudes">\n    <!-- <p>{{listaSolicitudes | json}}</p> -->\n    <ion-card (click)="abrirDetalleSolicitudCreada(i)" class="sombreadoTarjetaSolicitudCliente separarTarjetas ajusteTarjeta" *ngFor="let solicitud of listaSolicitudes; let i = index;">\n      <img src="/assets/imgs/creada.jpg" />\n      <div class="card-title">{{solicitud.servicioId.nombre}}</div>\n      <div class="card-subtitle">{{solicitud.fechaSolicitud | formatofecha}}</div>\n      <div class="fondoOscuroCard"></div>\n      <!-- <div class="card-subtitle">{{ item.descripcion }}</div> -->\n    </ion-card>\n  </div>\n\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\Solicitudes\contact.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_2__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_3__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */]])
    ], SolicitudPage);
    return SolicitudPage;
}());

//# sourceMappingURL=contact.js.map

/***/ }),

/***/ 169:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InicioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__listaservicios_listaservicios__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var InicioPage = /** @class */ (function () {
    function InicioPage(navCtrl, http, loadingCtrl) {
        this.navCtrl = navCtrl;
        this.http = http;
        this.loadingCtrl = loadingCtrl;
        this.obtenerValoresCategoria = [];
        this.error = [];
    }
    InicioPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        this.loading();
        // alert(this.http.getConfiguracion());
        this.http.getDataResource("obtenerCategorias").subscribe(function (datos) {
            console.log(datos.body);
            var bodyResponse = datos.body;
            var iterable = bodyResponse;
            _this.obtenerValoresCategoria = iterable;
            for (var index = 0; index < iterable.length; index++) {
                var cantidadServicios = iterable[index].servicioList.length;
                iterable[index].cantidadServicios = cantidadServicios;
            }
        }, function (err) {
            console.log(err);
            _this.error.push(err.message);
            _this.error.push(err.name);
            _this.error.push(err.status);
            _this.error.push(err.url);
            _this.loader.dismiss();
        }, function () {
            _this.loader.dismiss();
        });
    };
    InicioPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    InicioPage.prototype.listarServicios = function (indice) {
        console.log(this.obtenerValoresCategoria[indice]);
        var serviciosAsociados = this.obtenerValoresCategoria[indice].servicioList;
        if (serviciosAsociados.length > 0) {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__listaservicios_listaservicios__["a" /* ListaserviciosPage */], { servicios: serviciosAsociados }, {
                animation: "ios-transition"
            });
        }
    };
    InicioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-home',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\Inicio\home.html"*/'<ion-header>\n  <ion-navbar color="materialDesign">\n    <ion-title>Categorías</ion-title>\n  </ion-navbar>\n</ion-header>\n\n<ion-content class="separado card-background-page">\n  <ion-card (click)="listarServicios(indice)" class="sombreadoTarjetaCategorias separarTarjetas ajusteTarjeta" *ngFor="let item of obtenerValoresCategoria; let indice = index;">\n    <img src="/assets/imgs/banner2.png" />\n    <div class="card-title">{{item.nombre}}</div>\n    <div class="card-subtitle">{{ item.cantidadServicios }} servicios</div>\n\n    <div class="fondoOscuroCard"></div>\n    <!-- <div class="card-subtitle">{{ item.descripcion }}</div> -->\n  </ion-card>\n\n\n  <div *ngIf="error.length > 0">\n    <div>message: {{error[0]}}</div>\n    <div style="color: red; font-weight: bold;">name: {{error[1]}}</div>\n    <div>status: {{error[2]}}</div>\n    <div>URL: {{error[3]}}</div>\n  </div>\n\n\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\Inicio\home.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], InicioPage);
    return InicioPage;
}());

//# sourceMappingURL=home.js.map

/***/ }),

/***/ 170:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListaserviciosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detallesolicitud_detallesolicitud__ = __webpack_require__(171);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ListaserviciosPage = /** @class */ (function () {
    function ListaserviciosPage(navCtrl, navParams, modal) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.modal = modal;
        this.parametros = [];
    }
    ListaserviciosPage.prototype.ionViewWillEnter = function () {
        this.parametros = this.navParams.get('servicios');
        console.log("Estos son los parametros", this.parametros);
    };
    ListaserviciosPage.prototype.abrirModal = function (detalle) {
        var modalDetalle = this.modal.create("DetalleservicioPage", { descripcion: detalle });
        modalDetalle.present();
    };
    ListaserviciosPage.prototype.solicitarServicio = function (indice) {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_2__detallesolicitud_detallesolicitud__["a" /* DetallesolicitudPage */], { servicio: this.parametros[indice] }, {
            animation: "ios-transition"
        });
    };
    ListaserviciosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-listaservicios',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\listaservicios\listaservicios.html"*/'<ion-header>\n\n  <ion-navbar color="materialDesign">\n    <ion-title>Servicios</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card class="bordeado" *ngFor="let param of parametros; let indice = index;">\n    <img src="./assets/imgs/banner.jpg" />\n    <ion-card-content>\n      <ion-card-title class="alinearTituloTarjeta">\n        <b>{{param.nombre}}</b>\n      </ion-card-title>\n      <p>\n        {{param.descripcion}}\n        <!-- Decripción del servicio -->\n      </p>\n    </ion-card-content>\n    <div class="espaciadoDerechoBtn">\n      <ion-row>\n        <ion-col offset-7 col-5>\n          <button ion-button icon-start clear color="botonDetalles" (click)="solicitarServicio(indice)">\n            <ion-icon name="md-send"></ion-icon>\n            Solicitar\n          </button>\n        </ion-col>\n\n        <!-- <ion-col col-4>\n          <button ion-button icon-start clear color="botonDetalles" (click)="abrirModal(param.detalle)">\n            <ion-icon name="md-list"></ion-icon>\n            Detalles\n          </button>\n        </ion-col> -->\n      </ion-row>\n    </div>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\listaservicios\listaservicios.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */]])
    ], ListaserviciosPage);
    return ListaserviciosPage;
}());

//# sourceMappingURL=listaservicios.js.map

/***/ }),

/***/ 171:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetallesolicitudPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__proveedor_cercano_proveedor_cercano__ = __webpack_require__(172);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__clases_validarFormulario__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__clases_solicitud__ = __webpack_require__(284);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__ = __webpack_require__(84);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var DetallesolicitudPage = /** @class */ (function () {
    function DetallesolicitudPage(params, navCtrl, fb, modal, storage, geolocalizacion) {
        this.params = params;
        this.navCtrl = navCtrl;
        this.fb = fb;
        this.modal = modal;
        this.storage = storage;
        this.geolocalizacion = geolocalizacion;
        this.fechaActual = {};
        this.datosGeolocalizacion = {};
        this.pinSeleccionado = false;
        this.direccionCompleta = "";
        this.formulario = this.fb.group({
            descripcion: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            fechaSolicitud: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            direccion: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required],
            horaSolicitud: ['', __WEBPACK_IMPORTED_MODULE_2__angular_forms__["f" /* Validators */].required]
        });
    }
    DetallesolicitudPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        this.geolocalizacion.getCurrentPosition().then(function (posicion) {
            _this.datosGeolocalizacion.latitud = posicion.coords.latitude;
            _this.datosGeolocalizacion.longitud = posicion.coords.longitude;
            _this.pinSeleccionado = true;
            console.log("coordenadas obtenidas-> ", posicion.coords);
        }).catch(function (err) {
            console.error(err);
        });
    };
    DetallesolicitudPage.prototype.ionViewWillEnter = function () {
        this.servicio = this.params.get('servicio');
        var date = new Date();
        this.fechaActual.anioMinimo = date.getFullYear();
        this.fechaActual.anioMaximo = date.getFullYear() + "-12-31";
    };
    DetallesolicitudPage.prototype.abrirModal = function () {
        var _this = this;
        console.log("abriendoModalMapa->> ", this.datosGeolocalizacion);
        var modalMapa = this.modal.create('MapasPage', {
            pin: this.pinSeleccionado,
            latitud: this.datosGeolocalizacion.latitud,
            longitud: this.datosGeolocalizacion.longitud,
            direccionCompleta: this.direccionCompleta
        });
        modalMapa.present();
        modalMapa.onDidDismiss(function (datos) {
            console.log("coordenadasRecuperadas", datos);
            if (datos !== undefined) {
                _this.datosGeolocalizacion = datos;
                _this.formulario.get('direccion').setValue(datos.label);
                _this.pinSeleccionado = datos.pin;
                _this.direccionCompleta = datos.label;
            }
        });
    };
    DetallesolicitudPage.prototype.resize = function () {
        this.textAreaSolicitud._native.nativeElement.style.height = this.textAreaSolicitud._native.nativeElement.scrollHeight + 'px';
    };
    DetallesolicitudPage.prototype.SoloLectura = function () {
        return true;
    };
    DetallesolicitudPage.prototype.solicitarServicio = function () {
        var _this = this;
        // this.navCtrl.push(ProveedorCercanoPage, {}, {
        //   animation: "ios-transition"
        // });
        var validaciones = new __WEBPACK_IMPORTED_MODULE_4__clases_validarFormulario__["a" /* validarFormularios */]();
        this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
        var isValid = validaciones.isValidFormulario(this.formulario.controls);
        if (isValid) {
            var solicitudBody_1 = new __WEBPACK_IMPORTED_MODULE_5__clases_solicitud__["a" /* Solicitud */]();
            this.storage.get("datosUsuario").then(function (data) {
                solicitudBody_1.setSolicitudBody(_this.formulario.value, _this.datosGeolocalizacion, _this.servicio, data.usuarioId);
                _this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__proveedor_cercano_proveedor_cercano__["a" /* ProveedorCercanoPage */], { solicitud: solicitudBody_1.getSolicitudBody() }, {
                    animation: "ios-transition"
                });
            });
        }
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('textAreaSolicitud'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* TextInput */])
    ], DetallesolicitudPage.prototype, "textAreaSolicitud", void 0);
    DetallesolicitudPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detallesolicitud',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\detallesolicitud\detallesolicitud.html"*/'<ion-header>\n\n  <ion-navbar color="materialDesign">\n    <ion-title>Solicitar servicio</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-card class="sombreadoTarjeta">\n    <ion-card-content>\n      <p class="subtitulo">Ingrese los siguientes datos para realizar una solicitud a uno o varios proveedores cercanos</p>\n      <br>\n      <ion-list [formGroup]="formulario">\n\n        <ion-item class="separadoFormulario">\n          <ion-textarea id="textArea" #textAreaSolicitud (keyup)="resize()" maxLength="200" rows="1" [ngClass]="{mensajeError: (formulario.controls[\'descripcion\'].invalid && (formulario.controls[\'descripcion\'].dirty || formulario.controls[\'descripcion\'].touched))}"\n            formControlName="descripcion" class="usuarioInput" placeholder="Descripción de la solicitud"></ion-textarea>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'descripcion\'].invalid && (formulario.controls[\'descripcion\'].dirty || formulario.controls[\'descripcion\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n\n        <ion-item class="separadoFormulario">\n          <ion-input [readonly]="SoloLectura()" [ngClass]="{mensajeError: (formulario.controls[\'direccion\'].invalid && (formulario.controls[\'direccion\'].dirty || formulario.controls[\'direccion\'].touched))}"\n            formControlName="direccion" class="usuarioInput" type="text" placeholder="Dirección"></ion-input>\n        </ion-item>\n        <div>\n          <!-- Botón de la ventana modal -->\n          <button (click)="abrirModal()" class="ubicarBoton" ion-button icon-start color="colorPrimario">\n            <ion-icon name="search"></ion-icon>\n          </button>\n          <div *ngIf="formulario.controls[\'direccion\'].invalid && (formulario.controls[\'direccion\'].dirty || formulario.controls[\'direccion\'].touched)"\n            class="colorMensaje">\n            Error: Campo requerido\n          </div>\n        </div>\n\n        <ion-item class="separadoFormulario usuarioInput" [ngClass]="{mensajeError: (formulario.controls[\'fechaSolicitud\'].invalid && (formulario.controls[\'fechaSolicitud\'].dirty || formulario.controls[\'fechaSolicitud\'].touched))}" >\n          <ion-datetime class="" cancelText="Cancelar" doneText="Seleccionar" displayFormat="DD-MM-YYYY" formControlName="fechaSolicitud" placeholder="Fecha solicitud" min="{{fechaActual.anioMinimo}}" max="{{fechaActual.anioMaximo}}"></ion-datetime>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'fechaSolicitud\'].invalid && (formulario.controls[\'fechaSolicitud\'].dirty || formulario.controls[\'fechaSolicitud\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n\n\n        <ion-item class="separadoFormulario usuarioInput" [ngClass]="{mensajeError: (formulario.controls[\'horaSolicitud\'].invalid && (formulario.controls[\'horaSolicitud\'].dirty || formulario.controls[\'horaSolicitud\'].touched))}" >\n          <ion-datetime class="" cancelText="Cancelar" doneText="Seleccionar" displayFormat="hh:mm a" pickerFormat="hh:mm A" formControlName="horaSolicitud" placeholder="Hora solicitud"></ion-datetime>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'horaSolicitud\'].invalid && (formulario.controls[\'horaSolicitud\'].dirty || formulario.controls[\'horaSolicitud\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n\n\n        <!-- <ion-item class="separadoFormulario">\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'fechaSolicitud\'].invalid && (formulario.controls[\'fechaSolicitud\'].dirty || formulario.controls[\'fechaSolicitud\'].touched))}"\n            formControlName="fechaSolicitud" class="usuarioInput" type="text" placeholder="fechaSolicitud"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'fechaSolicitud\'].invalid && (formulario.controls[\'fechaSolicitud\'].dirty || formulario.controls[\'fechaSolicitud\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div> -->\n      </ion-list>\n      <div class="separado ubicacionBotones separadoAbajo">\n        <button ion-button color="colorPrimario" (click)="solicitarServicio()">Solicitar</button>\n      </div>\n\n    </ion-card-content>\n  </ion-card>\n\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\detallesolicitud\detallesolicitud.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__ionic_native_geolocation__["a" /* Geolocation */]])
    ], DetallesolicitudPage);
    return DetallesolicitudPage;
}());

//# sourceMappingURL=detallesolicitud.js.map

/***/ }),

/***/ 172:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProveedorCercanoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_storage__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(50);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var ProveedorCercanoPage = /** @class */ (function () {
    function ProveedorCercanoPage(navParams, loadingCtrl, navCrt, servicio, alert, storage) {
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.navCrt = navCrt;
        this.servicio = servicio;
        this.alert = alert;
        this.storage = storage;
        this.direccionesObtenidas = [];
        this.latitud = "";
        this.longitud = "";
        this.solicitudCliente = {};
        this.listaProveedores = [];
    }
    ProveedorCercanoPage.prototype.notificarTodosProveedores = function () {
        console.log("Cotización individual de proveedores realizada");
        this.solicitudCliente.usuarioIdProveedor = null;
        this.cotizarProveedores();
    };
    ProveedorCercanoPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    ProveedorCercanoPage.prototype.conversionCoordenadas = function () {
        var latitud = "" + this.solicitudCliente.latitud;
        var longitud = "" + this.solicitudCliente.longitud;
        this.solicitudCliente.latitud = latitud.replace(".", ",");
        this.solicitudCliente.longitud = longitud.replace(".", ",");
    };
    ProveedorCercanoPage.prototype.ionViewDidLoad = function () {
        var _this = this;
        console.log('Cargando mapa...');
        this.solicitudCliente = this.navParams.get("solicitud");
        this.conversionCoordenadas();
        console.log("SolicitudCliente por parámetros ---->>> ", this.solicitudCliente);
        this.loading();
        var longitud = "" + this.solicitudCliente.longitud;
        var latitud = "" + this.solicitudCliente.latitud;
        var servicio = this.solicitudCliente.servicioId.servicioId;
        var parametros = "?longitud=" + longitud.replace(".", ",") + "&latitud=" + latitud.replace(".", ",") + "&servicioId=" + servicio;
        this.servicio.getDataResource("obtenerProveedoresCercanos" + parametros).subscribe(function (proveedores) {
            console.log("Estos son los proveedores->> ", proveedores);
            if (proveedores.status == 200) {
                var conversion = proveedores.body;
                _this.listaProveedores = conversion;
                _this.construirMapa();
            }
        }, function (err) {
            console.error(err);
        });
    };
    ProveedorCercanoPage.prototype.construirMapa = function () {
        this.platform = new H.service.Platform({
            app_id: 'nhOznJTavPjLeA0U3dGS',
            app_code: 'qBiDEC4S0UqZPi7Ou5X1UQ',
            //useCIT: false,
            useHTTPS: true
        });
        this.defaultLayers = this.platform.createDefaultLayers({
            tileSize: 512,
            ppi: 320
        });
        this.group = new H.map.Group();
        this.map = new H.Map(document.getElementById('map'), this.defaultLayers.normal.map, {
            center: { lat: 4.6022608, lng: -74.0897502 },
            zoom: 12
        });
        this.map.addObject(this.group);
        this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));
        this.ui = H.ui.UI.createDefault(this.map, this.defaultLayers);
        console.log(this.ui);
        console.log(this.behavior);
        this.ubicarProveedoresMapa();
    };
    ProveedorCercanoPage.prototype.ubicarProveedoresMapa = function () {
        var group = this.group;
        group.addEventListener('tap', function (evt) {
            console.log("evento del tap ---> ", evt.target.getData());
            $('#tarjetaProveedor').data('informacion', evt.target.getData());
            $('#tarjetaProveedor').click();
        }, false);
        this.addMarkerToGroup(group);
        this.loader.dismiss();
    };
    ProveedorCercanoPage.prototype.tarjetaProveedor = function () {
        var _this = this;
        this.informacion = $('#tarjetaProveedor').data('informacion');
        var alert = this.alert.create({
            title: 'Proveedor',
            subTitle: this.informacion.nombres + " " + this.informacion.apellidos,
            buttons: [{
                    text: 'COTIZAR',
                    handler: function () {
                        console.log("Usuario lo ha cotizado");
                        _this.cotizarProveedores();
                    }
                }, {
                    text: 'CANCELAR',
                    role: 'cancel',
                    handler: function () {
                        console.log("acción cancelada");
                    }
                }]
        });
        alert.present();
    };
    ProveedorCercanoPage.prototype.cotizarProveedores = function () {
        var _this = this;
        this.loading();
        console.log("Cotización proveedores realizada");
        if (this.solicitudCliente.usuarioIdProveedor !== null) {
            if (this.solicitudCliente.usuarioIdProveedor.usuarioId == null)
                this.solicitudCliente.usuarioIdProveedor.usuarioId = this.informacion.usuarioId;
        }
        this.servicio.sendDataResource(this.solicitudCliente, "crearSolicitud").subscribe(function (solProveedor) {
            var listadoSolicitudesLocales = [];
            console.info("Enviando datos solicitud proveedores -> ", solProveedor);
            _this.storage.get("solicitudesCliente").then(function (data) {
                if (data == null) {
                    listadoSolicitudesLocales.push(solProveedor);
                    _this.storage.set("solicitudesCliente", listadoSolicitudesLocales);
                }
                else {
                    listadoSolicitudesLocales = data;
                    listadoSolicitudesLocales.push(solProveedor);
                    _this.storage.set("solicitudesCliente", listadoSolicitudesLocales);
                }
                _this.loader.dismiss();
                _this.navCrt.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */], { solicitudCreada: 1 });
            }).catch(function (err) { return console.log(err); });
        }, function (err) {
            console.error(err);
            _this.loader.dismiss();
        });
    };
    ProveedorCercanoPage.prototype.addMarkerToGroup = function (group) {
        var imagenUsuario = document.getElementById('imagenIconoUsuario');
        var iterador = this.listaProveedores;
        var marker, latitud, longitud;
        for (var index = 0; index < iterador.length; index++) {
            latitud = +iterador[index].coordenadaId.latitud.replace(",", ".");
            longitud = +iterador[index].coordenadaId.longitud.replace(",", ".");
            marker = new H.map.Marker({
                lat: latitud,
                lng: longitud
            }, { icon: new H.map.Icon(imagenUsuario) });
            marker.setData(iterador[index]);
            group.addObject(marker);
        }
    };
    ProveedorCercanoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-proveedor-cercano',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\proveedor-cercano\proveedor-cercano.html"*/'<ion-header>\n\n  <ion-navbar color="materialDesign">\n    <ion-title>Proveedores cercanos</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <ion-fab left bottom>\n    <button (click)="notificarTodosProveedores()" ion-fab color="danger">\n      <ion-icon name="logo-buffer"></ion-icon>\n    </button>\n  </ion-fab>\n  <div id="map" style="width: 470px; height: 840px"></div>\n  <!-- <img class="tamanioImagen" id="imagenIconoUsuario" src="assets/imgs/" alt="Archivo no encontrado"> -->\n  <img class="tamanioImagen" id="imagenIconoUsuario" src="assets/imgs/proveedor.png" alt="Archivo no encontrado">\n  <input type="hidden" id="tarjetaProveedor" (click)="tarjetaProveedor()">\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\proveedor-cercano\proveedor-cercano.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* NavParams */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["g" /* LoadingController */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["i" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */],
            __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_3__ionic_storage__["b" /* Storage */]])
    ], ProveedorCercanoPage);
    return ProveedorCercanoPage;
}());

//# sourceMappingURL=proveedor-cercano.js.map

/***/ }),

/***/ 174:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleServicioCreadoPageModule", function() { return DetalleServicioCreadoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detalle_servicio_creado__ = __webpack_require__(292);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(175);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var DetalleServicioCreadoPageModule = /** @class */ (function () {
    function DetalleServicioCreadoPageModule() {
    }
    DetalleServicioCreadoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detalle_servicio_creado__["a" /* DetalleServicioCreadoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detalle_servicio_creado__["a" /* DetalleServicioCreadoPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], DetalleServicioCreadoPageModule);
    return DetalleServicioCreadoPageModule;
}());

//# sourceMappingURL=detalle-servicio-creado.module.js.map

/***/ }),

/***/ 175:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__formatofecha_formatofecha__ = __webpack_require__(294);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__formatofecha_formatofecha__["a" /* FormatofechaPipe */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__formatofecha_formatofecha__["a" /* FormatofechaPipe */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 176:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetallesolicitudPageModule", function() { return DetallesolicitudPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detallesolicitud__ = __webpack_require__(171);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetallesolicitudPageModule = /** @class */ (function () {
    function DetallesolicitudPageModule() {
    }
    DetallesolicitudPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detallesolicitud__["a" /* DetallesolicitudPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detallesolicitud__["a" /* DetallesolicitudPage */]),
            ],
        })
    ], DetallesolicitudPageModule);
    return DetallesolicitudPageModule;
}());

//# sourceMappingURL=detallesolicitud.module.js.map

/***/ }),

/***/ 177:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "InicioSesionPageModule", function() { return InicioSesionPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__inicio_sesion__ = __webpack_require__(49);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var InicioSesionPageModule = /** @class */ (function () {
    function InicioSesionPageModule() {
    }
    InicioSesionPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__inicio_sesion__["a" /* InicioSesionPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__inicio_sesion__["a" /* InicioSesionPage */]),
            ],
        })
    ], InicioSesionPageModule);
    return InicioSesionPageModule;
}());

//# sourceMappingURL=inicio-sesion.module.js.map

/***/ }),

/***/ 178:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ListaserviciosPageModule", function() { return ListaserviciosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__listaservicios__ = __webpack_require__(170);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ListaserviciosPageModule = /** @class */ (function () {
    function ListaserviciosPageModule() {
    }
    ListaserviciosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__listaservicios__["a" /* ListaserviciosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__listaservicios__["a" /* ListaserviciosPage */]),
            ],
        })
    ], ListaserviciosPageModule);
    return ListaserviciosPageModule;
}());

//# sourceMappingURL=listaservicios.module.js.map

/***/ }),

/***/ 179:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProveedorCercanoPageModule", function() { return ProveedorCercanoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__proveedor_cercano__ = __webpack_require__(172);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProveedorCercanoPageModule = /** @class */ (function () {
    function ProveedorCercanoPageModule() {
    }
    ProveedorCercanoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__proveedor_cercano__["a" /* ProveedorCercanoPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__proveedor_cercano__["a" /* ProveedorCercanoPage */]),
            ],
        })
    ], ProveedorCercanoPageModule);
    return ProveedorCercanoPageModule;
}());

//# sourceMappingURL=proveedor-cercano.module.js.map

/***/ }),

/***/ 180:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RegistroUsuarioPageModule", function() { return RegistroUsuarioPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__registro_usuario__ = __webpack_require__(166);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var RegistroUsuarioPageModule = /** @class */ (function () {
    function RegistroUsuarioPageModule() {
    }
    RegistroUsuarioPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__registro_usuario__["a" /* RegistroUsuarioPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__registro_usuario__["a" /* RegistroUsuarioPage */]),
            ],
        })
    ], RegistroUsuarioPageModule);
    return RegistroUsuarioPageModule;
}());

//# sourceMappingURL=registro-usuario.module.js.map

/***/ }),

/***/ 222:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(223);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(243);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 23:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ServiciosServicioGeneralProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_timeout__ = __webpack_require__(272);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_timeout___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_rxjs_add_operator_timeout__);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ServiciosServicioGeneralProvider = /** @class */ (function () {
    function ServiciosServicioGeneralProvider(http) {
        this.http = http;
        this.protocolo = "http://";
        this.aplicacion = "";
        console.log('Llamando servicio...');
    }
    ServiciosServicioGeneralProvider.prototype.getDataResource = function (enlace, id) {
        if (id === void 0) { id = ""; }
        return this.http.get(this.enlaceGenerico + enlace + id, { observe: 'response' }).timeout(10000);
    };
    ServiciosServicioGeneralProvider.prototype.getMasivo = function (enlace, id) {
        if (id === void 0) { id = ""; }
        return this.http.get(this.enlaceGenerico + enlace + id);
    };
    ServiciosServicioGeneralProvider.prototype.sendDataResource = function (body, enlace) {
        if (enlace === void 0) { enlace = ""; }
        var cabecera = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().set('Content-Type', 'application/json; charset=utf-8');
        return this.http.post(this.enlaceGenerico + enlace, body, { headers: cabecera });
    };
    ServiciosServicioGeneralProvider.prototype.updateDataResource = function (id, body, cadena) {
        if (cadena === void 0) { cadena = ""; }
        var cabecera = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().set('Content-Type', 'application/json; charset=utf-8');
        return this.http.put((this.enlaceGenerico + cadena + "/" + id), body, { headers: cabecera });
    };
    ServiciosServicioGeneralProvider.prototype.updateDataResourceSinId = function (body, cadena) {
        if (cadena === void 0) { cadena = ""; }
        var cabecera = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpHeaders */]().set('Content-Type', 'application/json; charset=utf-8');
        return this.http.put((this.enlaceGenerico + cadena), body, { headers: cabecera });
    };
    ServiciosServicioGeneralProvider.prototype.deleteDataResource = function (enlace, id) {
        return this.http.delete((this.enlaceGenerico + enlace + "/" + id));
    };
    ServiciosServicioGeneralProvider.prototype.setConfiguracion = function (host, puerto) {
        this.hots = host;
        this.puerto = puerto;
        this.enlaceGenerico = this.protocolo + this.hots + this.puerto;
    };
    ServiciosServicioGeneralProvider.prototype.getConfiguracion = function () {
        return this.enlaceGenerico;
    };
    ServiciosServicioGeneralProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["A" /* Injectable */])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], ServiciosServicioGeneralProvider);
    return ServiciosServicioGeneralProvider;
}());

//# sourceMappingURL=servicios-servicio-general.js.map

/***/ }),

/***/ 243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__ = __webpack_require__(33);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__app_component__ = __webpack_require__(312);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_Configuraciones_about__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__pages_Solicitudes_contact_module__ = __webpack_require__(313);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_inicio_sesion_inicio_sesion_module__ = __webpack_require__(177);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__pages_registro_usuario_registro_usuario_module__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__pages_listaservicios_listaservicios_module__ = __webpack_require__(178);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__pages_detallesolicitud_detallesolicitud_module__ = __webpack_require__(176);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__pages_Inicio_home__ = __webpack_require__(169);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__pages_proveedor_cercano_proveedor_cercano_module__ = __webpack_require__(179);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__pages_administrador_administrador_module__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__pages_detalle_servicio_creado_detalle_servicio_creado_module__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__angular_common_http__ = __webpack_require__(164);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__ionic_native_status_bar__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__ionic_native_splash_screen__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_19__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_20__ionic_storage__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_21__ionic_native_geolocation__ = __webpack_require__(84);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_22__ionic_native_file__ = __webpack_require__(86);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};























var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_Configuraciones_about__["a" /* ConfiguracionPage */],
                // SolicitudPage,
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_Inicio_home__["a" /* InicioPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/administrador/administrador.module#AdministradorPageModule', name: 'AdministradorPage', segment: 'administrador', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detalle-servicio-creado/detalle-servicio-creado.module#DetalleServicioCreadoPageModule', name: 'DetalleServicioCreadoPage', segment: 'detalle-servicio-creado', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detalleservicio/detalleservicio.module#DetalleservicioPageModule', name: 'DetalleservicioPage', segment: 'detalleservicio', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/detallesolicitud/detallesolicitud.module#DetallesolicitudPageModule', name: 'DetallesolicitudPage', segment: 'detallesolicitud', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/inicio-sesion/inicio-sesion.module#InicioSesionPageModule', name: 'InicioSesionPage', segment: 'inicio-sesion', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/listaservicios/listaservicios.module#ListaserviciosPageModule', name: 'ListaserviciosPage', segment: 'listaservicios', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/mapas/mapas.module#MapasPageModule', name: 'MapasPage', segment: 'mapas', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/proveedor-cercano/proveedor-cercano.module#ProveedorCercanoPageModule', name: 'ProveedorCercanoPage', segment: 'proveedor-cercano', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/proveedor-servicios/proveedor-servicios.module#ProveedorServiciosPageModule', name: 'ProveedorServiciosPage', segment: 'proveedor-servicios', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/registro-usuario/registro-usuario.module#RegistroUsuarioPageModule', name: 'RegistroUsuarioPage', segment: 'registro-usuario', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_15__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_16__angular_forms__["e" /* ReactiveFormsModule */],
                __WEBPACK_IMPORTED_MODULE_10__pages_detallesolicitud_detallesolicitud_module__["DetallesolicitudPageModule"],
                __WEBPACK_IMPORTED_MODULE_6__pages_inicio_sesion_inicio_sesion_module__["InicioSesionPageModule"],
                __WEBPACK_IMPORTED_MODULE_9__pages_listaservicios_listaservicios_module__["ListaserviciosPageModule"],
                __WEBPACK_IMPORTED_MODULE_8__pages_registro_usuario_registro_usuario_module__["RegistroUsuarioPageModule"],
                __WEBPACK_IMPORTED_MODULE_12__pages_proveedor_cercano_proveedor_cercano_module__["ProveedorCercanoPageModule"],
                __WEBPACK_IMPORTED_MODULE_13__pages_administrador_administrador_module__["AdministradorPageModule"],
                __WEBPACK_IMPORTED_MODULE_14__pages_detalle_servicio_creado_detalle_servicio_creado_module__["DetalleServicioCreadoPageModule"],
                __WEBPACK_IMPORTED_MODULE_5__pages_Solicitudes_contact_module__["a" /* SolicitudPageModule */],
                __WEBPACK_IMPORTED_MODULE_20__ionic_storage__["a" /* IonicStorageModule */].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_3__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_4__pages_Configuraciones_about__["a" /* ConfiguracionPage */],
                // SolicitudPage,
                __WEBPACK_IMPORTED_MODULE_7__pages_tabs_tabs__["a" /* TabsPage */],
                __WEBPACK_IMPORTED_MODULE_11__pages_Inicio_home__["a" /* InicioPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_17__ionic_native_status_bar__["a" /* StatusBar */],
                __WEBPACK_IMPORTED_MODULE_18__ionic_native_splash_screen__["a" /* SplashScreen */],
                { provide: __WEBPACK_IMPORTED_MODULE_0__angular_core__["u" /* ErrorHandler */], useClass: __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["d" /* IonicErrorHandler */] },
                __WEBPACK_IMPORTED_MODULE_19__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */],
                __WEBPACK_IMPORTED_MODULE_21__ionic_native_geolocation__["a" /* Geolocation */],
                __WEBPACK_IMPORTED_MODULE_22__ionic_native_file__["a" /* File */]
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 284:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Solicitud; });
var Solicitud = /** @class */ (function () {
    function Solicitud() {
        this.solicitudBody = {
            descripcion: "",
            longitud: "",
            latitud: "",
            direccion: "",
            fechaSolicitud: "",
            servicioId: {
                servicioId: null
            },
            usuarioIdProveedor: {
                usuarioId: null
            },
            usuarioIdCliente: {
                usuarioId: null
            }
        };
    }
    Solicitud.prototype.setSolicitudBody = function (formulario, geolocalizacion, servicio, usuarioId) {
        this.solicitudBody.descripcion = formulario.descripcion;
        this.solicitudBody.direccion = formulario.direccion.trim();
        this.solicitudBody.fechaSolicitud = formulario.fechaSolicitud + "T" + formulario.horaSolicitud + ":36.000Z";
        this.solicitudBody.latitud = geolocalizacion.latitud;
        this.solicitudBody.longitud = geolocalizacion.longitud;
        this.solicitudBody.servicioId.servicioId = +servicio.servicioId;
        this.solicitudBody.usuarioIdCliente.usuarioId = usuarioId;
    };
    Solicitud.prototype.getSolicitudBody = function () {
        return this.solicitudBody;
    };
    return Solicitud;
}());

//# sourceMappingURL=solicitud.js.map

/***/ }),

/***/ 292:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalleServicioCreadoPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__clases_cotizacion__ = __webpack_require__(293);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__clases_validarFormulario__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_storage__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var DetalleServicioCreadoPage = /** @class */ (function () {
    function DetalleServicioCreadoPage(navCtrl, navParams, view, fb, storage, servicio, loadingCtrl, toast) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.fb = fb;
        this.storage = storage;
        this.servicio = servicio;
        this.loadingCtrl = loadingCtrl;
        this.toast = toast;
        this.listaCotizaciones = [];
        this.detalleServicio = {
            descripcion: "",
            nombre: "",
            fechaSolicitud: "",
            direccion: "",
            servicioId: null,
            cotizacionList: []
        };
        this.mostrarAccionesCliente = true;
        this.mostrarbadge = true;
        this.numeroSolicitudes = 0;
        this.mensajeCotizacionesVacias = false;
        this.tituloCotizaciones = "Cotizaciones recibidas";
        this.nombreServicio = "";
        this.detalleServicioCompleto = {};
        this.datosSession = {
            solicitudId: null,
            usuarioIdProveedor: null
        };
        this.mostrarBotones = true;
        this.formulario = this.fb.group({
            costoServicio: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            fechaSolicitud: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            horaSolicitud: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required],
            duracion: ['', __WEBPACK_IMPORTED_MODULE_3__angular_forms__["f" /* Validators */].required]
        });
    }
    DetalleServicioCreadoPage.prototype.ionViewDidLoad = function () {
        console.log('detalle solicitud cliente creada');
        this.detalleServicioCompleto = this.navParams.get("modalServicioDetail");
        this.usuarioSession = this.navParams.get("usuarioSession");
        this.detalleServicio.nombre = this.detalleServicioCompleto.servicioId.nombre;
        this.detalleServicio.descripcion = this.detalleServicioCompleto.descripcion;
        this.detalleServicio.fechaSolicitud = this.detalleServicioCompleto.fechaSolicitud;
        this.detalleServicio.direccion = this.detalleServicioCompleto.direccion;
        this.detalleServicio.servicioId = this.detalleServicioCompleto.servicioId.servicioId;
        this.detalleServicio.cotizacionList = this.detalleServicioCompleto.cotizacionList;
        // this.storage.get("datosUsuario").then(usuarioSession => {
        //   this.datosSession.usuarioIdProveedor = usuarioSession.usuarioId;
        // }).catch(err => console.error(err));
        console.log("detalleDelServicio --> ", this.detalleServicio);
        console.log("DetalleServicioCompleto --> ", this.detalleServicioCompleto);
        this.renderizarPaginaPorUsuario(this.detalleServicio.cotizacionList);
    };
    DetalleServicioCreadoPage.prototype.renderizarPaginaPorUsuario = function (cotizaciones) {
        var _this = this;
        this.storage.get("datosUsuario").then(function (datos) {
            _this.datosSession.usuarioIdProveedor = datos.usuarioId;
            if (datos !== null && datos !== undefined) {
                if (datos.rolId.rolId == 1 || datos.rolId.descripcion.toUpperCase() == "CLIENTE") {
                    // this.buscarCotizaciones();
                    _this.listadoCotizaciones(cotizaciones);
                }
                else {
                    _this.mostrarAccionesCliente = false;
                    _this.mostrarbadge = false;
                    _this.tituloCotizaciones = "Realizar Cotización";
                }
            }
        }).catch(function (err) { return console.error(err); });
    };
    DetalleServicioCreadoPage.prototype.listadoCotizaciones = function (cotizaciones) {
        console.log("Listado de cotizaciones --> ", cotizaciones);
        if (this.detalleServicioCompleto.estado.toUpperCase() == "ACEPTADA")
            this.mostrarBotones = false;
        if (cotizaciones.length >= 1) {
            this.numeroSolicitudes = cotizaciones.length;
            this.listaCotizaciones = cotizaciones.filter(function (data) { return data.estado.toUpperCase() == "CREADA"; });
            this.mensajeCotizacionesVacias = false;
        }
        else {
            this.mensajeCotizacionesVacias = true;
        }
    };
    DetalleServicioCreadoPage.prototype.aceptarCotizacion = function (index) {
        var _this = this;
        this.loading();
        var body = {
            cotizacionId: this.detalleServicioCompleto.cotizacionList[index].cotizacionId,
            solicitudId: {
                solicitudId: this.detalleServicioCompleto.solicitudId
            }
        };
        this.servicio.updateDataResourceSinId(body, "aceptarCotizacion").subscribe(function (enviado) {
            console.log("cotización aceptada ---> ", enviado);
            _this.crearToast("Cotización aceptada satisfactoriamente");
            _this.loader.dismiss();
            _this.consultarCotizacionesPorServicio(_this.detalleServicioCompleto.solicitudId);
        }, function (err) {
            console.error(err);
            _this.loader.dismiss();
            _this.crearToast("Error ocurrido al momento de aceptar una cotización");
        });
    };
    DetalleServicioCreadoPage.prototype.consultarCotizacionesPorServicio = function (solicitudId) {
        var _this = this;
        if (this.usuarioSession == 1) {
            this.servicio.getDataResource("obtenerCotizacionesPorSolicitud", "/" + solicitudId).subscribe(function (datos) {
                if (datos.status == 200 && datos.ok) {
                    var datosObtenidos = datos.body;
                    var arreglodatos = datosObtenidos;
                    if (datosObtenidos !== null && datosObtenidos !== undefined) {
                        console.log("Cotizaciones por solicitud---> ", datos);
                        if (arreglodatos[0].estado.toUpperCase() == "ACEPTADA")
                            _this.mostrarBotones = false;
                    }
                }
            }, function (err) {
                console.error(err);
            });
        }
    };
    DetalleServicioCreadoPage.prototype.rechazarCotizacion = function (index) {
        var _this = this;
        this.loading();
        var body = {
            cotizacionId: this.detalleServicioCompleto.cotizacionList[index].cotizacionId
        };
        this.servicio.updateDataResourceSinId(body, "rechazarCotizacion").subscribe(function (enviado) {
            console.log("cotización rechazada ---> ", enviado);
            _this.crearToast("Cotización rechazada satisfactoriamente");
            _this.loader.dismiss();
            // this.consultarCotizacionesPorServicio(this.detalleServicioCompleto.solicitudId);
            _this.eliminarCotizacionesRechazadas(index);
        }, function (err) {
            console.error(err);
            _this.loader.dismiss();
            _this.crearToast("Error ocurrido al momento de reachazar una cotización");
        });
    };
    DetalleServicioCreadoPage.prototype.eliminarCotizacionesRechazadas = function (index) {
        this.listaCotizaciones.splice(index, 1);
        console.log("Cotizaciones rechazadas eliminadas");
        this.numeroSolicitudes = this.listaCotizaciones.length;
    };
    DetalleServicioCreadoPage.prototype.crearToast = function (mensaje) {
        var toast = this.toast.create({
            message: mensaje,
            duration: 3000,
            position: 'top'
        });
        toast.present();
    };
    DetalleServicioCreadoPage.prototype.cerrarModal = function () {
        this.view.dismiss();
    };
    DetalleServicioCreadoPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    DetalleServicioCreadoPage.prototype.cotizarServicio = function () {
        var _this = this;
        this.loading();
        var cotizacion = new __WEBPACK_IMPORTED_MODULE_0__clases_cotizacion__["a" /* Cotizacion */]();
        if (this.validarCostoInput()) {
            var camposFormulario = this.formulario.value;
            this.datosSession.solicitudId = this.detalleServicioCompleto.solicitudId;
            cotizacion.setCotizacionBody(camposFormulario, this.datosSession);
            var bodyCotizacion = cotizacion.getCotizacionBody();
            console.log("Enviando solicitud ---> ", bodyCotizacion);
            this.servicio.sendDataResource(bodyCotizacion, "crearCotizacion").subscribe(function (data) {
                console.log("Cotizacion creada --> ", data);
                _this.loader.dismiss();
                var toast = _this.toast.create({
                    message: '¡Se ha creado la cotización satisfactoriamente!',
                    duration: 3000,
                    position: 'top'
                });
                toast.present();
                _this.view.dismiss();
            }, function (err) {
                console.error(err);
                _this.loader.dismiss();
            });
        }
        else
            this.loader.dismiss();
    };
    DetalleServicioCreadoPage.prototype.validarCostoInput = function () {
        var validaciones = new __WEBPACK_IMPORTED_MODULE_4__clases_validarFormulario__["a" /* validarFormularios */]();
        this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
        var isValid = validaciones.isValidFormulario(this.formulario.controls);
        return isValid;
    };
    DetalleServicioCreadoPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-detalle-servicio-creado',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\detalle-servicio-creado\detalle-servicio-creado.html"*/'<ion-header>\n    <ion-navbar color="materialDesign">\n        <ion-toolbar>\n            <ion-buttons end>\n                <button ion-button icon-only color="royal" (click)="cerrarModal()">\n                    <ion-icon name="close"></ion-icon>\n                </button>\n            </ion-buttons>\n            <ion-title>Servicio solicitado</ion-title>\n        </ion-toolbar>\n    </ion-navbar>\n</ion-header>\n\n<ion-content>\n\n    <div>\n        <img style="z-index: -1; position: absolute;" src="/assets/imgs/creada.jpg" alt="imagen no encontrada" width=100%\n            height=20%>\n    </div>\n    <div style="width: 90%; display:table; height: 55px; background-color: rgba(165, 42, 42, 0.993); z-index: 2; margin-top: 22%; margin-right: 5%; margin-left: 5%;">\n        <div style="font-size: 18px; color: white; text-align: center; display: table-cell; vertical-align: middle">\n            {{detalleServicio.nombre}}\n        </div>\n    </div>\n\n    <ion-list style="margin-top: 2%">\n        <ion-item>\n            <ion-icon name="list" item-start></ion-icon>\n            {{detalleServicio.descripcion}}\n        </ion-item>\n        <ion-item>\n            <ion-icon name="calendar" item-start></ion-icon>\n            {{detalleServicio.fechaSolicitud | formatofecha}}\n        </ion-item>\n        <ion-item>\n            <ion-icon name="pin" item-start></ion-icon>\n            {{detalleServicio.direccion }}\n        </ion-item>\n    </ion-list>\n\n    <ion-item style="background-color: rgba(165, 42, 42, 0.993); color: white">\n        <label class="espaciadoLateral">{{tituloCotizaciones}}</label>\n        <ion-badge *ngIf="mostrarbadge" item-end color="dark">{{numeroSolicitudes}} cotizaciones</ion-badge>\n    </ion-item>\n    <br>\n    <p *ngIf="mensajeCotizacionesVacias" style="text-align:center;"><b>No se han encontrado cotizaciones</b></p>\n\n    <!-- Propiedades del cliente -->\n    <div *ngIf="mostrarAccionesCliente">\n        <ion-card class="sombreadoCard" *ngFor="let cotizaciones of listaCotizaciones; let i = index;">\n            <ion-card-header>\n                <ion-list>\n                    <ion-item>\n                        <ion-avatar item-start>\n                            <img src="/assets/imgs/perfilProveedor3.png">\n                        </ion-avatar>\n                        <h2>{{cotizaciones.usuarioIdProveedor.nombres}} {{cotizaciones.usuarioIdProveedor.apellidos}}</h2>\n                        <p>Proveedor de servicios</p>\n                    </ion-item>\n                </ion-list>\n            </ion-card-header>\n            <hr>\n            <ion-list>\n                <button ion-item>\n                    <ion-icon name="calendar" item-start></ion-icon>\n                    {{cotizaciones.fechaEstimada | formatofecha}}\n                </button>\n\n                <button ion-item>\n                    <ion-icon name="logo-usd" item-start></ion-icon>\n                    {{cotizaciones.valor}}\n                </button>\n\n                <button ion-item>\n                    <ion-icon name="alarm" item-start></ion-icon>\n                    {{cotizaciones.duracion}} hora(s)\n                </button>\n\n            </ion-list>\n            <hr>\n            <div *ngIf="mostrarBotones">\n                <button ion-button icon-start clear color="verdozo" (click)="aceptarCotizacion(i)">\n                    <ion-icon name="thumbs-up"></ion-icon>\n                    Aceptar\n                </button>\n                <button ion-button icon-start clear color="danger" (click)="rechazarCotizacion(i)">\n                    <ion-icon name="thumbs-down"></ion-icon>\n                    rechazar\n                </button>\n            </div>\n        </ion-card>\n    </div>\n\n\n    <!-- Propiedades del proveedor -->\n    <ion-card *ngIf="!mostrarAccionesCliente" [formGroup]="formulario" class="sombreadoCard">\n        <ion-card-content>\n            <p>Ingresa el valor que estimas para llevar a cabo el trabajo solicitado</p>\n            <ion-list class="formulario">\n                <ion-item class="separado">\n                    <ion-input [ngClass]="{mensajeError: (formulario.controls[\'costoServicio\'].invalid && (formulario.controls[\'costoServicio\'].dirty || formulario.controls[\'costoServicio\'].touched))}"\n                        formControlName="costoServicio" class="usuarioInput" type="number" placeholder="Costo servicio ($)"\n                        minlength="4" maxlength="6"></ion-input>\n                </ion-item>\n                <div *ngIf="formulario.controls[\'costoServicio\'].invalid && (formulario.controls[\'costoServicio\'].dirty || formulario.controls[\'costoServicio\'].touched)"\n                    class="colorMensaje">\n                    Error: Campo requerido\n                </div>\n\n                <ion-item class="separadoFormulario usuarioInput" [ngClass]="{mensajeError: (formulario.controls[\'fechaSolicitud\'].invalid && (formulario.controls[\'fechaSolicitud\'].dirty || formulario.controls[\'fechaSolicitud\'].touched))}">\n                    <ion-datetime class="" cancelText="Cancelar" doneText="Seleccionar" displayFormat="DD-MM-YYYY"\n                        formControlName="fechaSolicitud" placeholder="Fecha estimada" min="2018" max="2018"></ion-datetime>\n                </ion-item>\n                <div *ngIf="formulario.controls[\'fechaSolicitud\'].invalid && (formulario.controls[\'fechaSolicitud\'].dirty || formulario.controls[\'fechaSolicitud\'].touched)"\n                    class="colorMensaje">\n                    Error: Campo requerido\n                </div>\n\n\n                <ion-item class="separadoFormulario usuarioInput" [ngClass]="{mensajeError: (formulario.controls[\'horaSolicitud\'].invalid && (formulario.controls[\'horaSolicitud\'].dirty || formulario.controls[\'horaSolicitud\'].touched))}">\n                    <ion-datetime class="" cancelText="Cancelar" doneText="Seleccionar" displayFormat="hh:mm a"\n                        pickerFormat="hh:mm A" formControlName="horaSolicitud" placeholder="Hora estimada"></ion-datetime>\n                </ion-item>\n                <div *ngIf="formulario.controls[\'horaSolicitud\'].invalid && (formulario.controls[\'horaSolicitud\'].dirty || formulario.controls[\'horaSolicitud\'].touched)"\n                    class="colorMensaje">\n                    Error: Campo requerido\n                </div>\n\n                <ion-item class="separado">\n                    <ion-input [ngClass]="{mensajeError: (formulario.controls[\'duracion\'].invalid && (formulario.controls[\'duracion\'].dirty || formulario.controls[\'duracion\'].touched))}"\n                        formControlName="duracion" class="usuarioInput" type="number" placeholder="Duración (horas)"\n                        minlength="1" maxlength="1"></ion-input>\n                </ion-item>\n                <div *ngIf="formulario.controls[\'duracion\'].invalid && (formulario.controls[\'duracion\'].dirty || formulario.controls[\'duracion\'].touched)"\n                    class="colorMensaje">\n                    Error: Campo requerido\n                </div>\n\n            </ion-list>\n        </ion-card-content>\n        <hr>\n        <button ion-button icon-start clear color="verdozo" (click)="cotizarServicio()">\n            <ion-icon name="cash"></ion-icon>\n            Cotizar\n        </button>\n        <!-- <button ion-button icon-start clear color="danger">\n            <ion-icon name="thumbs-down"></ion-icon>\n            rechazar\n        </button> -->\n    </ion-card>\n\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\detalle-servicio-creado\detalle-servicio-creado.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_3__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_5__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_6__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ToastController */]])
    ], DetalleServicioCreadoPage);
    return DetalleServicioCreadoPage;
}());

//# sourceMappingURL=detalle-servicio-creado.js.map

/***/ }),

/***/ 293:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Cotizacion; });
var Cotizacion = /** @class */ (function () {
    function Cotizacion() {
        this.cotizacionBody = {
            valor: 0,
            fechaEstimada: "",
            duracion: 0,
            solicitudId: {
                solicitudId: null
            },
            usuarioIdProveedor: {
                usuarioId: null
            }
        };
    }
    Cotizacion.prototype.setCotizacionBody = function (formulario, datosSession) {
        this.cotizacionBody.valor = formulario.costoServicio;
        this.cotizacionBody.fechaEstimada = formulario.fechaSolicitud + "T" + formulario.horaSolicitud + ":36.000Z";
        this.cotizacionBody.duracion = formulario.duracion;
        this.cotizacionBody.solicitudId.solicitudId = datosSession.solicitudId;
        this.cotizacionBody.usuarioIdProveedor.usuarioId = datosSession.usuarioIdProveedor;
    };
    Cotizacion.prototype.getCotizacionBody = function () {
        return this.cotizacionBody;
    };
    return Cotizacion;
}());

//# sourceMappingURL=cotizacion.js.map

/***/ }),

/***/ 294:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FormatofechaPipe; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var FormatofechaPipe = /** @class */ (function () {
    function FormatofechaPipe() {
    }
    FormatofechaPipe.prototype.transform = function (value, args) {
        var obtenervalorFecha = value.substring(0, value.indexOf("."));
        obtenervalorFecha = obtenervalorFecha.replace("T", " - ");
        var nuevafecha = obtenervalorFecha.split("-");
        obtenervalorFecha = nuevafecha[2] + "-" + nuevafecha[1] + "-" + nuevafecha[0] + " " + nuevafecha[3];
        return obtenervalorFecha;
    };
    FormatofechaPipe = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["S" /* Pipe */])({
            name: 'formatofecha',
        })
    ], FormatofechaPipe);
    return FormatofechaPipe;
}());

//# sourceMappingURL=formatofecha.js.map

/***/ }),

/***/ 312:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(220);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__ = __webpack_require__(221);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_inicio_sesion_inicio_sesion__ = __webpack_require__(49);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__ = __webpack_require__(86);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var MyApp = /** @class */ (function () {
    function MyApp(platform, statusBar, splashScreen, file, storage, servicio, app) {
        var _this = this;
        this.file = file;
        this.storage = storage;
        this.servicio = servicio;
        this.app = app;
        platform.ready().then(function () {
            statusBar.styleDefault();
            splashScreen.hide();
            _this.entrarVistaSeleccionada();
        });
    }
    MyApp.prototype.entrarVistaSeleccionada = function () {
        var _this = this;
        this.storage.get("logueado").then(function (data) {
            _this.establecerEnlace(data);
        }).catch(function (err) {
            console.log(err);
        });
    };
    MyApp.prototype.establecerEnlace = function (data) {
        var ruta = this.file.dataDirectory + "configuracion";
        this.servicio.setConfiguracion("192.168.0.15:", "8092/");
        this.rootPage = __WEBPACK_IMPORTED_MODULE_4__pages_inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */];
        // this.file.readAsText(ruta, "rootHogarapp").then(leer => {
        //   let atributosConexion = leer.split("|");
        //   this.servicio.setConfiguracion(atributosConexion[0], atributosConexion[1]);
        //   if (data == null) this.rootPage = InicioSesionPage;
        //   else this.rootPage = TabsPage;
        // }).catch(err => {
        //   console.log(JSON.stringify(err));
        //   this.establecerConfiguracionApp();
        // });
    };
    MyApp.prototype.establecerConfiguracionApp = function () {
        var _this = this;
        var ruta = this.file.dataDirectory + "configuracion";
        var texto = "192.168.0.15:|8092/";
        this.file.checkDir(this.file.dataDirectory, 'configuracion').then(function (existencia) { }).catch(function (err) {
            _this.file.createDir(_this.file.dataDirectory, 'configuracion', true).then(function (directorio) {
                _this.file.checkDir(_this.file.dataDirectory, 'configuracion').then(function (existencia) {
                    _this.file.createFile(ruta, "rootHogarapp", true).then(function (crear) {
                        _this.file.writeFile(_this.file.dataDirectory + "configuracion", "rootHogarapp", texto, { replace: true }).then(function (escribir) {
                            // se recarga de nuevo la página automáticamente
                            _this.app.getRootNavs()[0].push(__WEBPACK_IMPORTED_MODULE_4__pages_inicio_sesion_inicio_sesion__["a" /* InicioSesionPage */]);
                        }).catch(function (err) { });
                    }).catch(function (err) { });
                }).catch(function (err) { });
            }).catch(function (err) { });
        });
    };
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\app\app.html"*/'<ion-nav [root]="rootPage"></ion-nav>\n'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\app\app.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* Platform */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */], __WEBPACK_IMPORTED_MODULE_3__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_5__ionic_native_file__["a" /* File */], __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */], __WEBPACK_IMPORTED_MODULE_7__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["b" /* App */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 313:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return SolicitudPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__contact__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__ = __webpack_require__(175);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var SolicitudPageModule = /** @class */ (function () {
    function SolicitudPageModule() {
    }
    SolicitudPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__contact__["a" /* SolicitudPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__contact__["a" /* SolicitudPage */]),
                __WEBPACK_IMPORTED_MODULE_3__pipes_pipes_module__["a" /* PipesModule */]
            ],
        })
    ], SolicitudPageModule);
    return SolicitudPageModule;
}());

//# sourceMappingURL=contact.module.js.map

/***/ }),

/***/ 43:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return validarFormularios; });
var validarFormularios = /** @class */ (function () {
    function validarFormularios() {
    }
    /**
   * @author Jorge Leiva
   * @version 1.0.0
   * @description Procedimiento para la validación de los formularios
   * @param formulario Formulario reactivo
   * @returns variable formulario
   */
    validarFormularios.prototype.validarFormulariosService = function (formulario) {
        var controles = formulario.controls;
        for (var x in controles) {
            controles[x].markAsDirty();
        }
        return controles;
    };
    /**
     * @author Jorge Leiva
     * @version 1.0.0
     * @description Procedimiento validar el formulario
     * @param controles Controles del formulario
     * @returns variable booleana
     */
    validarFormularios.prototype.isValidFormulario = function (controles) {
        var acumulador = true;
        for (var x in controles) {
            if (controles[x].invalid == true) {
                acumulador = false;
                break;
            }
        }
        return acumulador;
    };
    validarFormularios.prototype.crearAlerta = function (alert, titulo, subtitulo) {
        if (titulo === void 0) { titulo = ""; }
        if (subtitulo === void 0) { subtitulo = ""; }
        var alertas = alert.create({
            title: titulo,
            subTitle: subtitulo,
            buttons: ['ACEPTAR']
        });
        return alertas;
    };
    return validarFormularios;
}());

//# sourceMappingURL=validarFormulario.js.map

/***/ }),

/***/ 49:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return InicioSesionPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__registro_usuario_registro_usuario__ = __webpack_require__(166);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__tabs_tabs__ = __webpack_require__(50);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_forms__ = __webpack_require__(12);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__clases_validarFormulario__ = __webpack_require__(43);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ionic_storage__ = __webpack_require__(22);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__administrador_administrador__ = __webpack_require__(163);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};









var InicioSesionPage = /** @class */ (function () {
    function InicioSesionPage(navCtrl, navParams, servicio, fb, alert, loadingCtrl, storage) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.servicio = servicio;
        this.fb = fb;
        this.alert = alert;
        this.loadingCtrl = loadingCtrl;
        this.storage = storage;
        this.formulario = this.fb.group({
            usuario: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required],
            contrasenia: ['', __WEBPACK_IMPORTED_MODULE_5__angular_forms__["f" /* Validators */].required]
        });
    }
    InicioSesionPage.prototype.ionViewDidLoad = function () {
        console.log('Inicio de sesion cargado');
    };
    InicioSesionPage.prototype.registrarUsuario = function () {
        this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_3__registro_usuario_registro_usuario__["a" /* RegistroUsuarioPage */], {}, {
            animation: "ios-transition"
        });
    };
    InicioSesionPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    InicioSesionPage.prototype.cargarVistaAdministrador = function (datosUsuario, validaciones) {
        var _this = this;
        if (datosUsuario.usuario == "admin" && datosUsuario.contrasenia == "@dmin$$") {
            this.navCtrl.push(__WEBPACK_IMPORTED_MODULE_8__administrador_administrador__["a" /* AdministradorPage */]);
            this.loader.dismiss();
        }
        else {
            this.servicio.getDataResource("buscarUsuarioPorCorreo/", datosUsuario.usuario).subscribe(function (data) {
                var datos = data.body;
                console.log("usuario-> ", datos);
                if ((data.status == 200 && datos.usuarioId !== null) && (datos.correo == datosUsuario.usuario && datos.password == datosUsuario.contrasenia)) {
                    _this.storage.clear();
                    _this.storage.set('datosUsuario', datos);
                    _this.storage.set("logueado", 1);
                    _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_4__tabs_tabs__["a" /* TabsPage */], { tipoUsuario: datos.descripcionPerfil, solicitudCreada: 0 });
                }
                else {
                    var alert_1 = validaciones.crearAlerta(_this.alert, "Error de credenciales", "Los datos ingresados no son válidos");
                    alert_1.present();
                    _this.formulario.reset();
                }
            }, function (err) {
                console.log(err);
                _this.error = err;
                var alertError = validaciones.crearAlerta(_this.alert, "Error de conexión", err.message);
                alertError.present();
                _this.loader.dismiss();
            }, function () {
                _this.loader.dismiss();
            });
        }
    };
    InicioSesionPage.prototype.goVistaPrincipal = function () {
        // this.navCtrl.setRoot(TabsPage);
        var validaciones = new __WEBPACK_IMPORTED_MODULE_6__clases_validarFormulario__["a" /* validarFormularios */]();
        this.formulario.controls = validaciones.validarFormulariosService(this.formulario);
        var isValid = validaciones.isValidFormulario(this.formulario.controls);
        var datosUsuario = this.formulario.value;
        if (isValid) {
            this.loading();
            this.cargarVistaAdministrador(datosUsuario, validaciones);
        }
    };
    InicioSesionPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({
            selector: 'page-inicio-sesion',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\inicio-sesion\inicio-sesion.html"*/'<ion-content padding class="fondoComponente">\n  <ion-card class="sombreadoTarjeta posicionCentradaTarjeta" [formGroup]="formulario">\n    <ion-card-header class="colores">\n      HogarApp\n    </ion-card-header>\n    <ion-card-content>\n      <!-- <p>{{error | json}}</p> -->\n      <!-- <p style="color: red">{{exito | json}}</p> -->\n      <!-- <p></p> -->\n      <p class="subtitulo">Empieza a buscar proveedores que atiendan tus necesidades en el Hogar</p>\n      <br>\n      <ion-list class="formulario">\n\n        <ion-item class="separado">\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'usuario\'].invalid && (formulario.controls[\'usuario\'].dirty || formulario.controls[\'usuario\'].touched))}"\n            maxlength="40" formControlName="usuario" class="usuarioInput" type="text" placeholder="Correo electrónico"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'usuario\'].invalid && (formulario.controls[\'usuario\'].dirty || formulario.controls[\'usuario\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n\n        <ion-item class="separado">\n          <ion-icon name="heart"></ion-icon>\n          <ion-input [ngClass]="{mensajeError: (formulario.controls[\'contrasenia\'].invalid && (formulario.controls[\'contrasenia\'].dirty || formulario.controls[\'contrasenia\'].touched))}"\n            maxlength="40" formControlName="contrasenia" class="usuarioInput" type="password" placeholder="Contraseña"></ion-input>\n        </ion-item>\n        <div *ngIf="formulario.controls[\'contrasenia\'].invalid && (formulario.controls[\'contrasenia\'].dirty || formulario.controls[\'contrasenia\'].touched)"\n          class="colorMensaje">\n          Error: Campo requerido\n        </div>\n      </ion-list>\n\n      <div class="formulario separado ubicacionBotones separadoAbajo">\n        <button ion-button color="danger" (click)="registrarUsuario()">Registrarse</button>\n        <button ion-button color="colorPrimario" (click)="goVistaPrincipal()">Iniciar</button>\n      </div>\n\n\n    </ion-card-content>\n  </ion-card>\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\inicio-sesion\inicio-sesion.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_0__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */], __WEBPACK_IMPORTED_MODULE_5__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["g" /* LoadingController */], __WEBPACK_IMPORTED_MODULE_7__ionic_storage__["b" /* Storage */]])
    ], InicioSesionPage);
    return InicioSesionPage;
}());

//# sourceMappingURL=inicio-sesion.js.map

/***/ }),

/***/ 50:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return TabsPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__Configuraciones_about__ = __webpack_require__(167);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__Solicitudes_contact__ = __webpack_require__(168);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__Inicio_home__ = __webpack_require__(169);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var TabsPage = /** @class */ (function () {
    function TabsPage(navParams) {
        this.navParams = navParams;
        this.tab1Root = null;
        this.tab2Root = __WEBPACK_IMPORTED_MODULE_3__Solicitudes_contact__["a" /* SolicitudPage */];
        this.tab3Root = __WEBPACK_IMPORTED_MODULE_2__Configuraciones_about__["a" /* ConfiguracionPage */];
        this.tituloPaginaInicio = "Inicio";
        this.tituloPaginaSolicitudes = "Solicitudes";
        this.tituloPaginaConfiguracion = "Perfil";
        this.iconoServicio = "chatbubbles";
        this.selectedIndexTabs = 0;
    }
    TabsPage.prototype.ionViewDidLoad = function () {
        // Se ejecuta cuando se crea una nueva solicitud
        var selectedIndexTabs = this.navParams.get("solicitudCreada");
        if (selectedIndexTabs !== null)
            this.opcionesApp.select(selectedIndexTabs);
        var tipoUsuario = this.navParams.get("tipoUsuario");
        console.log("TipoUsuario", tipoUsuario);
        this.esValido = this.validarUsuario(tipoUsuario);
        if (this.esValido) {
            console.log("Entra como proveedor");
            this.iconoServicio = "paper";
            this.tituloPaginaSolicitudes = "Buzón";
            this.indice = 1;
        }
        else {
            console.log("entra como usuario");
        }
    };
    TabsPage.prototype.ionViewCanEnter = function () {
        var tipoUsuario = this.navParams.get("tipoUsuario");
        this.esValido = this.validarUsuario(tipoUsuario);
        if (!this.esValido) {
            this.tab1Root = __WEBPACK_IMPORTED_MODULE_4__Inicio_home__["a" /* InicioPage */];
        }
        else {
            this.opcionesApp._tabs[0].show = false;
        }
    };
    TabsPage.prototype.validarUsuario = function (tipoUsuario) {
        var respuesta = false;
        if (tipoUsuario == "1")
            respuesta = true; //tipo de usuario Proveedor
        return respuesta;
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["_8" /* ViewChild */])("opcionesApp"),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_0_ionic_angular__["l" /* Tabs */])
    ], TabsPage.prototype, "opcionesApp", void 0);
    TabsPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["m" /* Component */])({template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\tabs\tabs.html"*/'<ion-tabs color="dark" #opcionesApp selectedIndex = "{{indice}}">\n  <ion-tab [root]="tab1Root" tabTitle="{{tituloPaginaInicio}}" tabIcon="home"></ion-tab>\n  <ion-tab [root]="tab2Root" tabTitle="{{tituloPaginaSolicitudes}}" tabIcon="{{iconoServicio}}"></ion-tab>\n  <ion-tab [root]="tab3Root" tabTitle="{{tituloPaginaConfiguracion}}" tabIcon="md-person"></ion-tab>\n</ion-tabs>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\tabs\tabs.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0_ionic_angular__["j" /* NavParams */]])
    ], TabsPage);
    return TabsPage;
}());

//# sourceMappingURL=tabs.js.map

/***/ })

},[222]);
//# sourceMappingURL=main.js.map