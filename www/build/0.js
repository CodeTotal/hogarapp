webpackJsonp([0],{

/***/ 316:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ProveedorServiciosPageModule", function() { return ProveedorServiciosPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__proveedor_servicios__ = __webpack_require__(319);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var ProveedorServiciosPageModule = /** @class */ (function () {
    function ProveedorServiciosPageModule() {
    }
    ProveedorServiciosPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__proveedor_servicios__["a" /* ProveedorServiciosPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__proveedor_servicios__["a" /* ProveedorServiciosPage */]),
            ],
        })
    ], ProveedorServiciosPageModule);
    return ProveedorServiciosPageModule;
}());

//# sourceMappingURL=proveedor-servicios.module.js.map

/***/ }),

/***/ 319:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ProveedorServiciosPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_servicios_servicio_general_servicios_servicio_general__ = __webpack_require__(23);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var ProveedorServiciosPage = /** @class */ (function () {
    function ProveedorServiciosPage(navCtrl, navParams, view, servicio) {
        this.navCtrl = navCtrl;
        this.navParams = navParams;
        this.view = view;
        this.servicio = servicio;
        this.listaServicios = [];
        this.identificadoresSeervicio = {
            cantidadServicios: "",
            idServicios: []
        };
        this.botonGuardarServicio = false;
    }
    ProveedorServiciosPage.prototype.ionViewWillEnter = function () {
        var _this = this;
        var parametros = this.navParams.get("servicios");
        this.identificadoresSeervicio.idServicios = parametros;
        this.servicio.getDataResource("/obtenerCategorias").subscribe(function (data) {
            var bodyResponse = data.body;
            _this.listaServicios = _this.asignarChecked(bodyResponse, _this.identificadoresSeervicio.idServicios);
        }, function (err) { return console.error(err); });
    };
    ProveedorServiciosPage.prototype.asignarChecked = function (bodyResponse, parametros) {
        var _this = this;
        var respuesta = bodyResponse;
        respuesta.forEach(function (elemento) {
            elemento.servicioList.forEach(function (servicio) {
                if (parametros.length > 0) {
                    _this.botonGuardarServicio = true;
                    var validacion = parametros.some(function (data) { return data == servicio.servicioId; });
                    if (validacion)
                        servicio.checked = true;
                    else
                        servicio.checked = false;
                }
                else {
                    servicio.checked = false;
                }
            });
        });
        return respuesta;
    };
    ProveedorServiciosPage.prototype.guardarServicios = function () {
        this.cerrarModal();
    };
    ProveedorServiciosPage.prototype.cerrarModal = function () {
        this.identificadoresSeervicio.cantidadServicios = this.identificadoresSeervicio.idServicios.length;
        this.view.dismiss(this.identificadoresSeervicio);
    };
    ProveedorServiciosPage.prototype.cambioEstadoCheckBox = function (evento) {
        var existeServicio = this.identificadoresSeervicio.idServicios.findIndex(function (data) { return data == evento; });
        if (existeServicio !== -1)
            this.identificadoresSeervicio.idServicios.splice(existeServicio, 1);
        else
            this.identificadoresSeervicio.idServicios.push(+evento);
        if (this.identificadoresSeervicio.idServicios.length > 0)
            this.botonGuardarServicio = true;
        else
            this.botonGuardarServicio = false;
    };
    ProveedorServiciosPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-proveedor-servicios',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\proveedor-servicios\proveedor-servicios.html"*/'<ion-header>\n  <ion-navbar color="materialDesign">\n    <ion-toolbar>\n      <ion-buttons end>\n        <button ion-button icon-only color="royal" (click)="cerrarModal()">\n          <ion-icon name="close"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-title>Servicios</ion-title>\n    </ion-toolbar>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-fab left bottom *ngIf="botonGuardarServicio">\n    <button (click)="guardarServicios()" ion-fab color="botonDetalles">\n      <ion-icon name="add-circle"></ion-icon>\n    </button>\n  </ion-fab>\n\n  <ion-item-group>\n    <div *ngFor="let categorias of listaServicios; let i = index">\n      <ion-item-divider color="light">{{categorias.nombre}}</ion-item-divider>\n      <ion-item *ngFor="let servicios of categorias.servicioList">\n        <ion-label>{{servicios.descripcion}}</ion-label>\n        <ion-checkbox checked={{servicios.checked}} id="{{servicios.servicioId}}" color="dark" (ionChange)="cambioEstadoCheckBox($event._elementRef.nativeElement.id)"></ion-checkbox>\n      </ion-item>\n    </div>\n  </ion-item-group>\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\proveedor-servicios\proveedor-servicios.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["i" /* NavController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_2__providers_servicios_servicio_general_servicios_servicio_general__["a" /* ServiciosServicioGeneralProvider */]])
    ], ProveedorServiciosPage);
    return ProveedorServiciosPage;
}());

//# sourceMappingURL=proveedor-servicios.js.map

/***/ })

});
//# sourceMappingURL=0.js.map