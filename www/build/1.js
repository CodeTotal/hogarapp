webpackJsonp([1],{

/***/ 315:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "MapasPageModule", function() { return MapasPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__mapas__ = __webpack_require__(318);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var MapasPageModule = /** @class */ (function () {
    function MapasPageModule() {
    }
    MapasPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__mapas__["a" /* MapasPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__mapas__["a" /* MapasPage */]),
            ],
        })
    ], MapasPageModule);
    return MapasPageModule;
}());

//# sourceMappingURL=mapas.module.js.map

/***/ }),

/***/ 318:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MapasPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var MapasPage = /** @class */ (function () {
    function MapasPage(view, navParams, loadingCtrl) {
        this.view = view;
        this.navParams = navParams;
        this.loadingCtrl = loadingCtrl;
        this.direccionesObtenidas = [];
        this.latitud = "";
        this.longitud = "";
        this.objetoMapeo = {};
    }
    MapasPage.prototype.loading = function () {
        this.loader = this.loadingCtrl.create({
            content: "Espera..."
        });
        this.loader.present();
    };
    MapasPage.prototype.ionViewDidLoad = function () {
        console.log('Cargando mapa...');
        // this.objetoMapa();
        this.loading();
        var mapaDatosCargados = {
            pinSeleccionado: this.navParams.get("pin"),
            direccionCompleta: this.navParams.get("direccionCompleta"),
            latitud: this.navParams.get("latitud"),
            longitud: this.navParams.get("longitud"),
        };
        this.objetoMapeo = mapaDatosCargados;
        this.construirMapa();
    };
    MapasPage.prototype.cerrarModal = function () {
        this.view.dismiss();
    };
    MapasPage.prototype.enviarResponseMapa = function () {
        var suggestions = $('#enviarResponseMapa').data('valores');
        var filtro = suggestions.filter(function (data) {
            if (data.address.county == "Bogotá, D.C.") {
                var valorStreet = data.label.split(",");
                data.address.street = valorStreet[valorStreet.length - 1];
                return data;
            }
        });
        this.direccionesObtenidas = filtro;
    };
    MapasPage.prototype.responseAdressComplete = function () {
        var datos = this;
        $('#enviarResponseMapa').data('valores', datos.response.suggestions);
        $('#enviarResponseMapa').click();
    };
    MapasPage.prototype.responseAdressCompleteFailed = function () {
        console.log("Error encontrado en la obtención de los valores mapeados");
    };
    MapasPage.prototype.obtenerCoordenadas = function () {
        console.log("construye las coordenadas-->> ");
        this.latitud = $('#coordenadas').data('latitud');
        this.longitud = $('#coordenadas').data('longitud');
    };
    MapasPage.prototype.guardarCoordenadas = function () {
        var datos = {
            latitud: this.latitud,
            longitud: this.longitud,
            label: $('#coordenadas').data('label'),
            pin: this.objetoMapeo.pinSeleccionado
        };
        this.view.dismiss(datos);
    };
    MapasPage.prototype.buscarDireccion = function (evento) {
        var event = evento, textBox = this.searchAdress._native.nativeElement;
        console.log(event);
        var AUTOCOMPLETION_URL = 'https://autocomplete.geocoder.api.here.com/6.2/suggest.json', ajaxRequest = new XMLHttpRequest(), query = '';
        ajaxRequest.addEventListener("load", this.responseAdressComplete);
        ajaxRequest.addEventListener("error", this.responseAdressCompleteFailed);
        ajaxRequest.responseType = "json";
        function autoCompleteListener(textBox) {
            if (query != textBox.value) {
                if (textBox.value.length >= 1) {
                    var params = '?' +
                        'query=' + encodeURIComponent(textBox.value) + // The search text which is the basis of the query
                        '&country=' + 'COL' +
                        '&beginHighlight=' + encodeURIComponent('') + //  Mark the beginning of the match in a token. 
                        '&endHighlight=' + encodeURIComponent('') + //  Mark the end of the match in a token. 
                        '&maxresults=5' + // The upper limit the for number of suggestions to be included
                        '&app_id=' + 'nhOznJTavPjLeA0U3dGS' +
                        '&app_code=' + 'qBiDEC4S0UqZPi7Ou5X1UQ';
                    ajaxRequest.open('GET', AUTOCOMPLETION_URL + params);
                    ajaxRequest.send();
                }
            }
            query = textBox.value;
        }
        autoCompleteListener(textBox);
    };
    MapasPage.prototype.manejadorDragableMaker = function () {
        var dragableMaker = $('#manejadorDragableMaker').data();
        var mapa = dragableMaker.mapa;
        var behavior = dragableMaker.behavior;
        var platform = dragableMaker.platform;
        var group = dragableMaker.group;
        mapa.addEventListener('dragstart', function (ev) {
            var target = ev.target;
            if (target instanceof H.map.Marker) {
                behavior.disable();
            }
        }, false);
        mapa.addEventListener('dragend', function (ev) {
            var target = ev.target;
            if (target instanceof mapsjs.map.Marker) {
                behavior.enable();
            }
            var coordenada = mapa.screenToGeo(ev.currentPointer.viewportX, ev.currentPointer.viewportY);
            $('#reverseGeocodeAddress').data({
                platform: platform,
                coordenada: coordenada
            });
            $('#reverseGeocodeAddress').click();
        }, false);
        mapa.addEventListener('drag', function (ev) {
            var target = ev.target, pointer = ev.currentPointer;
            if (target instanceof mapsjs.map.Marker) {
                target.setPosition(mapa.screenToGeo(pointer.viewportX, pointer.viewportY));
            }
        }, false);
        mapa.setViewBounds(group.getBounds(), true);
        if (group.getObjects().length < 2)
            mapa.setZoom(15);
    };
    MapasPage.prototype.reverseGeocodeAddress = function () {
        var reverseGeocode = $('#reverseGeocodeAddress').data();
        var platform = reverseGeocode.platform;
        var coordenada = reverseGeocode.coordenada;
        var geocoder = platform.getGeocodingService(), parameters = {
            prox: coordenada.lat + "," + coordenada.lng + ",250",
            mode: 'retrieveAddresses',
            maxresults: '1',
            gen: '9'
        };
        geocoder.reverseGeocode(parameters, function (resultado) {
            console.log("Este es el resultado de la búsqueda", resultado);
            var data = resultado.Response.View[0].Result[0].Location.Address;
            var label = data.Label.split(",")[0] + " - " + data.District;
            $('#coordenadas').data({
                'latitud': coordenada.lat,
                'longitud': coordenada.lng,
                'label': label
            });
            $('#coordenadas').click();
        }, function (error) {
            console.log(error);
        });
    };
    MapasPage.prototype.limpiarMarcadoresMapa = function () {
        var group = $('#limpiarMarcadoresMapa').data('group');
        group.removeAll();
    };
    MapasPage.prototype.crearMarcadorMapa = function (data) {
        $('#limpiarMarcadoresMapa').data({ group: this.group });
        $('#limpiarMarcadoresMapa').click();
        this.objetoMapeo.pinSeleccionado = true;
        var imagenUsuario = document.getElementById('imagenIconoUsuario');
        var mapa = this.map;
        var group = this.group;
        var behavior = this.behavior;
        var platform = this.platform;
        this.direccionesObtenidas = [];
        this.searchAdress._native.nativeElement.value = "";
        var geocodingParameters = {
            locationId: data.locationId
        }, onResult = function (resultado) {
            console.log("ResultadoObtenidoGeocoder", resultado);
            var locations = resultado.Response.View[0].Result;
            var latitud = locations[0].Location.DisplayPosition.Latitude;
            var longitud = locations[0].Location.DisplayPosition.Longitude;
            var direccionCompleta = data.address.street + ", " + data.address.district + ", " + data.address.county;
            $('#coordenadas').data({
                'latitud': latitud,
                'longitud': longitud,
                'label': direccionCompleta
            });
            var marker = new H.map.Marker({
                lat: latitud,
                lng: longitud
            }, { icon: new H.map.Icon(imagenUsuario) });
            marker.draggable = true; //permitir el desplazamiento del marcador en el mapa
            group.addObject(marker);
            $('#manejadorDragableMaker').data({
                mapa: mapa,
                group: group,
                behavior: behavior,
                platform: platform
            });
            $('#manejadorDragableMaker').click();
            $('#coordenadas').click();
        }, onError = function (error) {
            console.log("errorGenerado", error);
        };
        this.geocoder.geocode(geocodingParameters, onResult, onError);
    };
    MapasPage.prototype.construirMapa = function () {
        this.platform = new H.service.Platform({
            app_id: 'nhOznJTavPjLeA0U3dGS',
            app_code: 'qBiDEC4S0UqZPi7Ou5X1UQ',
            // useCIT: false,
            useHTTPS: true
        });
        this.defaultLayers = this.platform.createDefaultLayers({
            tileSize: 512,
            ppi: 320
        });
        this.geocoder = this.platform.getGeocodingService();
        this.group = new H.map.Group();
        this.map = new H.Map(document.getElementById('map'), this.defaultLayers.normal.map, {
            // center: { lat: 4.6022608, lng: -74.0897502 },
            center: { lat: 4.6022608, lng: -74.0897502 },
            zoom: 11
            // , pixelRatio: 2
        });
        this.map.addObject(this.group);
        this.behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(this.map));
        this.ui = H.ui.UI.createDefault(this.map, this.defaultLayers);
        console.log("behavior", this.behavior);
        console.log(this.ui);
        // Decisión---> mostrar el pin si anteriormente se había seleccionado en el mapa
        if (this.objetoMapeo.pinSeleccionado) {
            this.pintarMaracadorInicial();
            console.log("Entra-> mapa cargado anteriormente");
        }
        this.loader.dismiss();
    };
    MapasPage.prototype.pintarMaracadorInicial = function () {
        var mapa = this.map;
        var group = this.group;
        var behavior = this.behavior;
        var platform = this.platform;
        var mapeo = this.objetoMapeo;
        var imagenUsuario = document.getElementById('imagenIconoUsuario');
        var coordenada = {
            lat: mapeo.latitud,
            lng: mapeo.longitud
        };
        if (mapeo.direccionCompleta == "") {
            $('#reverseGeocodeAddress').data({
                platform: platform,
                coordenada: coordenada
            });
            $('#reverseGeocodeAddress').click();
        }
        $('#coordenadas').data({
            'latitud': mapeo.latitud,
            'longitud': mapeo.longitud,
            'label': mapeo.direccionCompleta
        });
        var marker = new H.map.Marker({
            lat: mapeo.latitud,
            lng: mapeo.longitud
        }, { icon: new H.map.Icon(imagenUsuario) });
        marker.draggable = true; //permitir el desplazamiento del marcador en el mapa
        group.addObject(marker);
        $('#manejadorDragableMaker').data({
            mapa: mapa,
            group: group,
            behavior: behavior,
            platform: platform
        });
        $('#manejadorDragableMaker').click();
        $('#coordenadas').click();
    };
    MapasPage.prototype.objetoMapa = function () {
        function addDraggableMarker(map, behavior) {
            var imagenUsuario = document.getElementById('imagenIconoUsuario');
            console.log("Imagen", imagenUsuario);
            var marker = new H.map.Marker({
                lat: 4.6022608,
                lng: -74.0897502
            });
            marker.draggable = true;
            map.addObject(marker);
            map.addEventListener('dragstart', function (ev) {
                var target = ev.target;
                if (target instanceof H.map.Marker) {
                    behavior.disable();
                }
            }, false);
            map.addEventListener('dragend', function (ev) {
                var target = ev.target;
                console.log(target);
                if (target instanceof mapsjs.map.Marker) {
                    behavior.enable();
                }
            }, false);
            map.addEventListener('drag', function (ev) {
                var target = ev.target, pointer = ev.currentPointer;
                if (target instanceof mapsjs.map.Marker) {
                    target.setPosition(map.screenToGeo(pointer.viewportX, pointer.viewportY));
                }
            }, false);
        }
        // Configuración general del mapa
        var platform = new H.service.Platform({
            app_id: 'nhOznJTavPjLeA0U3dGS',
            app_code: 'qBiDEC4S0UqZPi7Ou5X1UQ',
            useHTTPS: true
        });
        var defaultLayers = platform.createDefaultLayers({});
        var map = new H.Map(document.getElementById('map'), defaultLayers.normal.map, {
            center: { lat: 4.6022608, lng: -74.0897502 },
            zoom: 12
        });
        var behavior = new H.mapevents.Behavior(new H.mapevents.MapEvents(map));
        var ui = H.ui.UI.createDefault(map, defaultLayers);
        console.log(ui);
        addDraggableMarker(map, behavior);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["_8" /* ViewChild */])('searchAdress'),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["m" /* TextInput */])
    ], MapasPage.prototype, "searchAdress", void 0);
    MapasPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-mapas',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\mapas\mapas.html"*/'<ion-header>\n  <ion-navbar color="materialDesign">\n    <ion-toolbar>\n      <ion-buttons end>\n        <button ion-button icon-only color="royal" (click)="cerrarModal()">\n          <ion-icon name="close"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-title>Dirección</ion-title>\n    </ion-toolbar>\n  </ion-navbar>\n</ion-header>\n\n\n<ion-content>\n  <ion-fab left bottom *ngIf="latitud !== \'\'">\n    <button (click)="guardarCoordenadas()" ion-fab color="botonDetalles"><ion-icon name="md-pin"></ion-icon></button>\n  </ion-fab>\n\n  <div id="map" style="width: 470px; height: 840px"></div>\n  <img class="tamanioImagen" id="imagenIconoUsuario" src="assets/imgs/pinMapa54.png" alt="Archivo no encontrado">\n  <ion-list class="posicionarInputBusqueda">\n    <ion-item class="">\n      <ion-input id="elementoBuscador" #searchAdress maxlength="40" type="text" placeholder="Buscar" (keyup)="buscarDireccion($event)"></ion-input>\n    </ion-item>\n  </ion-list>\n  <button class="ubicarBotonMapas" clear ion-button icon-start color="colorPrimario">\n    <ion-icon name="search"></ion-icon>\n  </button>\n  <input type="hidden" id="enviarResponseMapa" (click)="enviarResponseMapa()">\n  <input type="hidden" id="coordenadas" (click)="obtenerCoordenadas()">\n  <input type="hidden" id="manejadorDragableMaker" (click)="manejadorDragableMaker()">\n  <input type="hidden" id="reverseGeocodeAddress" (click)="reverseGeocodeAddress()">\n  <input type="hidden" id="limpiarMarcadoresMapa" (click)="limpiarMarcadoresMapa()">\n  \n\n  <ion-list class="posicionarListaBusqueda">\n    <button ion-item *ngFor="let direcciones of direccionesObtenidas; let i = index;" (click)="crearMarcadorMapa(direcciones)">\n      <p class="textoListaBusqueda">{{direcciones.address.county}}, {{direcciones.address.district}}, {{direcciones.address.street}}</p>\n    </button>\n  </ion-list>\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\mapas\mapas.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["g" /* LoadingController */]])
    ], MapasPage);
    return MapasPage;
}());

//# sourceMappingURL=mapas.js.map

/***/ })

});
//# sourceMappingURL=1.js.map