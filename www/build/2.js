webpackJsonp([2],{

/***/ 314:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DetalleservicioPageModule", function() { return DetalleservicioPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__detalleservicio__ = __webpack_require__(317);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



var DetalleservicioPageModule = /** @class */ (function () {
    function DetalleservicioPageModule() {
    }
    DetalleservicioPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["I" /* NgModule */])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_2__detalleservicio__["a" /* DetalleservicioPage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_2__detalleservicio__["a" /* DetalleservicioPage */]),
            ],
        })
    ], DetalleservicioPageModule);
    return DetalleservicioPageModule;
}());

//# sourceMappingURL=detalleservicio.module.js.map

/***/ }),

/***/ 317:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return DetalleservicioPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(0);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(6);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var DetalleservicioPage = /** @class */ (function () {
    function DetalleservicioPage(navParams, view) {
        this.navParams = navParams;
        this.view = view;
        this.descripcionServicio = "";
    }
    DetalleservicioPage.prototype.ionViewWillEnter = function () {
        this.descripcionServicio = this.navParams.get('descripcion');
    };
    DetalleservicioPage.prototype.cerrarModal = function () {
        this.view.dismiss();
    };
    DetalleservicioPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["m" /* Component */])({
            selector: 'page-detalleservicio',template:/*ion-inline-start:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\detalleservicio\detalleservicio.html"*/'<ion-header>\n\n  <ion-navbar color="materialDesign">\n    <ion-toolbar>\n      <ion-buttons end>\n        <button ion-button icon-only color="royal" (click)="cerrarModal()">\n          <ion-icon name="close"></ion-icon>\n        </button>\n      </ion-buttons>\n      <ion-title>Detalle</ion-title>\n    </ion-toolbar>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content padding>\n  <p>{{descripcionServicio}}</p>\n</ion-content>'/*ion-inline-end:"K:\1.DESARROLLO\1.APPS\4.HOGARAPP\Hogarapp\src\pages\detalleservicio\detalleservicio.html"*/,
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["o" /* ViewController */]])
    ], DetalleservicioPage);
    return DetalleservicioPage;
}());

//# sourceMappingURL=detalleservicio.js.map

/***/ })

});
//# sourceMappingURL=2.js.map